# Interlinked - Get A Real Job!
______________________________________________________
Interlinked is a job seeking and a job posting website.

There are two possibilities for a user to signed in, the first is as a Job Seeker and the second is as an employer.

Job Seekers would be able to apply for jobs that have been posted and also have their own CV made in Interlinked.

Employers are able to to  post jobs, view who has applied to their job posts, view their CVs and of course accepting or rejecting those applications.  
______________________________________________________
### Table of Contents:

[Functionality](#markdown-header-functionality)  
[Database](#markdown-header-database)  
[Setup](#markdown-header-setup)  
[References](#markdown-header-references)   
[Comments](#markdown-header-comments)  
______________________________________________________

#### Functionality

* Interlinked has fully functional register and log in features

![ScreenShot](assets/images/readme/1.PNG)

![ScreenShot](assets/images/readme/2.PNG)

* Once Logged In, the user will be shown their profile page

![ScreenShot](assets/images/readme/3.PNG)

* They would also be able to edit their profile

![ScreenShot](assets/images/readme/4.PNG)

* If the account is an employer account, they will be able to create a job

![ScreenShot](assets/images/readme/5.PNG)

* They would also be able to view the Job in full detail

![ScreenShot](assets/images/readme/18.PNG)

* Employers would also be able to delete job posts

![ScreenShot](assets/images/readme/19.PNG)

* If the account is a Job Seeker account, they will be able to apply for a job from the jobs list

![ScreenShot](assets/images/readme/6.PNG)

* They would also be able to view the status of their applications

![ScreenShot](assets/images/readme/7.PNG)

* On the other end, the employers will be able to see their applicants list

![ScreenShot](assets/images/readme/8.PNG)

* Employers will be able to view each of the applicant's CV

![ScreenShot](assets/images/readme/9.PNG)

![ScreenShot](assets/images/readme/10.PNG)

* Ultimately, they will have the ability to accept or reject applications

![ScreenShot](assets/images/readme/11.PNG)

* Job Seekers are able to add to their CV Page

![ScreenShot](assets/images/readme/12.PNG)

![ScreenShot](assets/images/readme/13.PNG)

![ScreenShot](assets/images/readme/14.PNG)

* Job Seekers are able to edit their existing CV details both updating and deleting.

![ScreenShot](assets/images/readme/15.PNG)

![ScreenShot](assets/images/readme/16.PNG)

![ScreenShot](assets/images/readme/17.PNG)


______________________________________________________
### Database

1. Entities    
    * There are 11 main entities
        * jobs
        * job_categories
        * job_industries
        * users
        * user_cv
        * user_education
        * user_experience
        * user_genders
        * user_referees
        * user_skills
        * user_titles  

    * There is one joining table
        * job_applications

2. Relationships  
    * Relationships
        * Users may have many CV elements (user_education, user_experience, user_referees, user_skills, user_cv)
        * Employer users may have many jobs
        * Users may have one user_title and user_gender
        * Jobs may have one job_category and job_industry
        * Users may have many job_applications
        * Jobs may have many job_applications

3. Schema

![Database Schema](assets/images/readme/database_schema.PNG)

______________________________________________________
### Setup

1. Download project

    * Clone the project into your directory of choice:  
    `git clone https://nicholasdaryl@bitbucket.org/D00206780/wp_ca4_nicholasdaryl_jamesfarrell.git`

2. Create Database

    * Create a PhpMyAdmin database 
    * Import the included [MySQL file](assets\sql\wp_ca4_nicholasdaryl_jamesfarrell.sql) to the database  

3. Configure Project

    * Create a config.php file at the project root
    * Copy the contents of [config.php.sample](config.php.sample) into the config file
    * Edit config.php (see [file](config.php.sample) for instructions)
    * Edit [.htaccess](.htaccess) (see [file](.htaccess) for instructions)

______________________________________________________
### References

1. Code Base  

    * The code base for this application was pulled from a GitLab repository, created by Shane Gavin.  
    * Shane discussed the Rapid MVC framework with us, as we developed it in class.
        * [GitLab Repo](https://gitlab.comp.dkit.ie/gavins/rapid-starter-project)

2. Stack Overflow

    * Stack Overflow helped us overcome some problems that we faced.  
    * References have been added throughout.

______________________________________________________
### Comments

1. General
    * It is overall not a very finished website. Home page is not implemented. Could have been more robust in few places or done in a better way

2. Reflections
    * The deleting of items does not pop up a confirm window to prevent from accidental deletes.
    * The view profile and view job details could be better

______________________________________________________