<?php

    try {

        # Include rapid
        require_once('lib/Rapid.php');

        # Create router
        $app = new \Rapid\Router();

        # Define Regex param patterns
        $entity_id = '(?<entity_id>[0-9]{1,20})';
        $operation = '(?<operation>[a-zA-Z]{1,})';

        # Get routes
        $app->GET('/',                          'ViewHomePage');
        $app->GET('/logout',                    'Logout');
        $app->GET('/cv',                        'ViewCV');
        $app->GET('/view_job',                  'ViewJob');

        $app->GET('/view_jobs_page',            'ViewJobsPage');
        $app->GET('/view_jobs_form',            'ViewJobsForm');
        $app->GET('/view_profile_page',         'ViewProfilePage');
        $app->GET('/view_profile_form',         'ViewProfileForm');
        $app->GET('/applications',              'ViewApplications');
        $app->GET('/processcv',                 'ProcessCVUpdate');
        $app->GET('/add_cv',                    'AddCV');
        $app->GET('/delete_job',                'ProcessJobsDelete');
        $app->GET('/apply_job',                 'ProcessAddJobApplication');
        $app->GET('/delete_application',        'ProcessDeleteJobApplication');
        $app->GET('/accept_application',        'ProcessAcceptJobApplication');
        $app->GET('/reject_application',        'ProcessRejectJobApplication');

        # Post routes
        $app->POST('/add_cv',                   'ProcessAddCV');
        $app->POST('/processcv',                'ProcessCVUpdate');
        $app->POST('/create_job',               'ProcessJobsCreate');
        $app->POST('/update_job',               'ProcessJobsUpdate');
        $app->POST('/update_profile',           'ProcessProfileUpdate');
        $app->POST('/login',                    'ProcessLoginForm');
        $app->POST('/register',                 'ProcessRegisterForm');

        # Validate login
        $app->GET('/api/validateLogin',         'api/ValidateLogin');
        $app->POST('/api/validateLogin',        'api/ValidateLogin');

        # Check if value exists
        $app->GET('/api/validateRegistration',  'api/ValidateRegistration');
        $app->POST('/api/validateRegistration', 'api/ValidateRegistration');

        # Validate registration
        $app->GET('/api/checkValueExists',      'api/CheckValueExists');
        $app->POST('/api/checkValueExists',     'api/CheckValueExists');

        # Process request
        $app->dispatch();

    } catch (\Rapid\RouteNotFoundException $e) {

        $e->getResponseObject()->render('main', 'error', [
        'error'         => '404 - Not Found!',
        'message'       => $e->getMessage(),
        ]);

        http_response_code(404); 
    
    } catch (ControllerNotFoundException $e) {

        $e->getResponseObject()->render('main', 'error', [
            'error'         => '404 - Not Found!',
            'message'       => $e->getMessage(),
        ]);

        http_response_code(404);

    } catch (RouteRedeclarationException $e) {

        $e->getResponseObject()->render('main', 'error', [
            'error'         => '404 - Not Found!',
            'message'       => $e->getMessage(),
        ]);

        http_response_code(404);

    } catch (ViewNotFoundException $e) {

        $e->getResponseObject()->render('main', 'error', [
            'error'         => '404 - Not Found!',
            'message'       => $e->getMessage(),
        ]);

        http_response_code(404);

    } catch (LayoutNotFoundException $e) {
        
        $e->getResponseObject()->render('main', 'error', [
            'error'         => '404 - Not Found!',
            'message'       => $e->getMessage(),
        ]);

        http_response_code(404);

    } catch (Exception $e) {

        http_response_code(500);
        exit();

    }
    
?>