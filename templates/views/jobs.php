<div class='container'>
    <?php if ($locals['jobs']) { ?>
        <?php foreach ($locals['jobs'] as $job) { ?>
            <div class='card col-md-6 mt-3 p-0'>
                <div class='row shadow-sm no-gutters'>
                    <div class='col-md-2 rounded-left bg-dark'>
                        <?php if ($job->get('user_id') === $locals['user_id']) { ?>
                            <a class='btn text-white' href='view_jobs_form?id=<?= $job->get('id') ?>&operation=update'><i class='fas fa-pen'></i></a>
                            <a class='btn text-white' href='delete_job?id=<?= $job->get('id') ?>'><i class='fas fa-trash-alt'></i></a>
                        <?php } ?>
                    </div>
                    <div class='col-md-9'>
                        <div class='card-header clearfix pb-1'>
                            <a href='view_job?id=<?= $job->get('id') ?>'><h5 class='float-left'><?= $job->get('title') ?></h5></a>
                            <h6 class='float-right'><?= $job->get('company') ?></h6>
                        </div>
                        <div class='card-body'>
                            <div class='row'>
                                <p class='col-md-10 card-text text-left text-truncate'><?= $job->get('description') ?></p>
                                <?php if (!$locals['account_type']) { ?>
                                    <a class='col-md-2' href='apply_job?job_id=<?= $job->get('id') ?>'><small>Apply</small></a>
                                <?php } ?>
                            </div>
                        </div>
                        <div class='card-footer clearfix'>
                            <small class='float-left text-muted'><?= $job->get('term') ?></small>
                            <small class='float-right text-muted'><?= $job->get('location') ?></small>
                        </div>
                    </div>
                    <div class='col-md-1 rounded-right bg-secondary'></div>
                </div>
            </div>
        <?php } ?>
    <?php } else { ?>
        <div>No results found!</div>
    <?php } ?>

    <?php if ($locals['account_type']) { ?>
        <div>
            <a href='view_jobs_form?operation=create'>Create job!</a>
        </div>
    <?php } ?>
</div>