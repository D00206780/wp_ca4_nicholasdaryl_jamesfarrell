<!-- Container -->
<div class='container col-md-6 shadow-sm p-3 mt-5 bg-white rounded'>

    <!-- Job Form -->    
    <form action='<?= $locals['operation'] ?? '' ?>_job?id=<?= $locals['job']['id'] ?? '' ?>' class='needs-validation' method='POST' novalidate>

        <!-- Errors -->
        <?php if ($locals['job_form_error']) { ?>
            <div class='form-group'>
                <div class='form-control-plaintext'><?= $locals['job_form_error'] ?></div>
            </div>
        <?php } ?>

        <!-- Title & Company -->
        <div class='form-row'>

            <!-- Job title -->
            <div class='form-group col-md-6'>
                <label for='title'>Title:</label>
                <input type='text' class='form-control' id='title' name='title' value='<?= $locals['job']['title'] ?? '' ?>' placeholder='What is the job title?' required autofocus>
                <div class='valid-feedback'>Looks good!</div>
            </div>
            
            <!-- Job company -->
            <div class='form-group col-md-6'>
                <label for='company'>Company:</label>
                <input type='text' class='form-control' id='company' name='company' value='<?= $locals['job']['company'] ?? '' ?>' placeholder='Who do you work for?' required autofocus>
            </div>
        </div>

        <!-- Job description -->
        <div class='form-group'>
            <label for='descripiton'>Description:</label>
            <textarea class='form-control' id='description' name='description' value='<?= $locals['job']['description'] ?? '' ?>' placeholder='Sell your job!' rows='3'></textarea>
        </div>

        <!-- Job location -->
        <div class='form-group'>
            <label for='location'>Location:</label>
            <input type='text' class='form-control' id='location' name='location' value='<?= $locals['job']['location'] ?? '' ?>' placeholder='Where is the job located?' required>
        </div>

        <!-- Term & Rate -->
        <div class='form-row'>

            <!-- Job term -->
            <div class='form-group col-md-6'>
                <label for='term'>Term:</label>
                <input type='text' class='form-control' id='term' name='term' value='<?= $locals['job']['term'] ?? '' ?>' placeholder='How long will the contract be?' required>
            </div>

            <!-- Job Rate -->
            <div class='form-group col-md-6'>
                <label for='rate'>Rate:</label>
                <input type='text' class='form-control' id='rate' name='rate' value='<?= $locals['job']['rate'] ?? '' ?>' placeholder='Salary or Wage?' required>
            </div>
        </div>

        <!-- Industry & Category -->
        <div class='form-row'>

            <!-- Industry -->
            <div class='form-group col-md-6'>

                <!-- Title -->
                <label for='industry_id'>Industry:</label>
                <select class='form-control' id='industry_id' name='industry_id'>

                    <!-- Output job category (if present), drawn from the database -->
                    <?php if ($locals['job']['industry_id'] && $locals['job']['industry_name']) { ?>
                        <option value='<?= $locals['job']['industry_id'] ?? '' ?>'><?= $locals['job']['industry_name'] ?? '' ?></option>
                    <?php } else { ?>
                        <option value='<?= NULL ?>'>Please Select</option>
                    <?php } ?>

                    <!-- Output list of industries, drawn from the database -->
                    <?php if ($locals['industries']) { ?>
                        <?php foreach ($locals['industries'] as $industry) { ?>
                            <option value='<?= $industry->get('id') ?>'><?= $industry->get('name') ?></option>
                        <?php } ?>
                    <?php } else { ?>
                        <option value='Error!'></option>
                    <?php }?>
                </select>
            </div>

            <!-- Category -->
            <div class='form-group col-md-6'>

                <!-- Title -->
                <label for='category_id'>Category:</label>
                <select class='form-control' id='category_id' name='category_id'>

                    <!-- Output job category (if present), drawn from the database -->
                    <?php if ($locals['job']['category_id'] && $locals['job']['category_name']) { ?>
                        <option value='<?= $locals['job']['category_id'] ?>'><?= $locals['job']['category_name'] ?></option>
                    <?php } else { ?>
                        <option value='<?= NULL ?>'>Please Select</option>
                    <?php } ?>

                    <!-- Output list of categories, drawn from the database -->
                    <?php if ($locals['categories']) { ?>
                        <?php foreach ($locals['categories'] as $category) { ?>
                            <option value='<?= $category->get('id') ?>'><?= $category->get('name') ?></option>
                        <?php } ?>
                    <?php } else { ?>
                        <option value='Error!'></option>
                    <?php }?>
                </select>
            </div>
        </div>

        <!-- Submit form -->
        <button type='submit' class='btn btn-primary'>Save Changes</button>
    </form>
</div>