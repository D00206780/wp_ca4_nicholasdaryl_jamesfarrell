<h1>Applications</h1>
<?php if (!empty($locals['jobApplications'])) { ?> 
    <?php foreach ($locals['jobApplications'] as $jobApplication) { ?>
        <div class='mainContentWindow__rowContainer'>
            <p><strong>Title:</strong> <?= $jobApplication['job']->get('title') ?></p>
            <p><strong>Job Description:</strong> <?= $jobApplication['job']->get('description') ?></p>
            
            <?php if ($jobApplication['status'] == 1) { ?>
                <p><strong>Status:</strong>Accepted</p>
            <?php } else if ($jobApplication['status'] == -1) { ?>
                <p><strong>Status:</strong>Rejected</p>
            <?php } else { ?>
                <p><strong>Status:</strong>Pending</p>
            <?php } ?>
        
            <a class='btn btn-danger' href='delete_application?id=<?= $jobApplication['job']->get('id') ?>'>Delete</a>
        </div>
    <?php } ?>      
<?php } else { ?>
    <p>No Applications have been made as of yet</p>
<?php } ?>