<div class='container'>
    <?php if ($locals['status']) { ?>
        <div class='col-md-12 mt-3 pt-3 pl-3 pr-3 pb-2 text-white bg-primary rounded'>
            <?php if ($locals['status'] === 'updated') { ?>
                <h5>Profile Updated!</h5>
            <?php } else { ?>
                <h5>No changes!</h5>
            <?php }?>
        </div>
    <?php } ?>
    <div class='card mt-3 p-0'>
        <img src='assets/images/card.png' class='card-img-top' alt='...'>
        <div class='row shadow-sm no-gutters'>
            <div class='card col-md-3'>
                <div class='card-header'>
                    <img src='assets/images/profile.png' class='col-md-10 offset-md-1' alt='...'>
                </div>
                <div class='card-body text-center'>
                    <div class='card-text'><?= $locals['user']['title_name'] . ' ' .  $locals['user']['first_name'] . ' ' . $locals['user']['last_name'] ?> </div>
                    <div class='card-text'><?= $locals['user']['gender_name'] ?? '' ?></div>
                    <div class='card-text'><?= $locals['user']['contact_number'] ?? '' ?></div>
                    <div class='card-text'><?= $locals['user']['email_address'] ?? '' ?></div>
                </div>
            </div>
            <div class='card col-md-8'>
                <div class='card-body text-center'>
                    <div class='card-text'><?= $locals['user']['address_line_1'] ?></div>
                    <div class='card-text'><?= $locals['user']['address_line_2'] ?></div>
                    <div class='card-text'><?= $locals['user']['town_city'] ?></div>
                    <div class='card-text'><?= $locals['user']['county'] ?></div>
                    <div class='card-text'><?= $locals['user']['country'] ?></div>
                </div>
                <div class='card-footer clearfix'>
                    <div><a href='view_profile_form'>Edit Details</a></div> 
                </div>
            </div>
            <div class='col-md-1 text-white bg-secondary'></div>
        </div>
    </div>
</div>