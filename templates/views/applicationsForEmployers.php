<h1>Applications</h1>
<?php if (!empty($locals['jobApplications'])) { ?> 
    <?php foreach ($locals['jobApplications'] as $jobApplication) { ?>
        <div class='mainContentWindow__rowContainer'>
            <p><strong>Job Title:</strong> <?= $jobApplication['job']->get('title') ?></p>
            <p><strong>Applicant Name:</strong> <?= $jobApplication['user']->get('first_name') ?> <?= $jobApplication['user']->get('last_name') ?></p>
            <a class='btn btn-success' href='accept_application?applicantUserId=<?= $jobApplication['user']->get('id') ?>&jobId=<?= $jobApplication['job']->get('id') ?>'>Accept</a>
            <a class='btn btn-danger' href='reject_application?applicantUserId=<?= $jobApplication['user']->get('id') ?>&jobId=<?= $jobApplication['job']->get('id') ?>'>Reject</a>
            <a style='float: right;' class='btn btn-info' href='cv?applicantUserId=<?= $jobApplication['user']->get('id') ?>'>View Applicant CV</a>
        </div>       
    <?php } ?>
<?php } else { ?>
    <p>No Applications have been made as of yet.</p>
<?php } ?>
