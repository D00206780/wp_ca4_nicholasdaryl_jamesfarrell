<form action='add_cv' method='post'>
    <h1>Professional experience</h1>
    <div class='mainContentWindow__rowContainer'>
        <div class='form-group col-md-6'>
            <p><strong><label for='experienceTerm'>Term: <label></strong></p>
            <p><input type='text' class='form-control' name='experienceTerm' required></p>
        </div>
        <div class='form-group col-md-6'>
            <p><strong><label for='experiencePosition'>Position: <label></strong></p>
            <p><input type='text' class='form-control' name='experiencePosition' required></p>
        </div>
        <div class='form-group col-md-6'>
            <p><strong><label for='experienceCompany'>Company Name: <label></strong></p>
            <p><input type='text' class='form-control' name='experienceCompany' required></p>
        </div>
        <div class='form-group col-md'>
            <p><strong><label for='experienceDescription'>Job Description: <label></strong></p>
            <textarea name='experienceDescription' class='form-control' rows='10' cols='100' required></textarea>
        </div>
        <div class='form-group col-md'>
            <p><input type='submit' class='btn btn-primary' value='Add'></p>
        </div>
    </div>
</form>