<!-- Container -->
<div class='container col-md-6 offset-md-3 shadow-sm p-3 mt-5 bg-white rounded'>

    <!-- User form -->
    <form action='update_profile' class='needs-validation' id='profile_form' method='POST' novalidate>
        
        <!-- Errors  -->
        <?php if ($locals['profile_form_error']) { ?>
            <div class='form-group'>
                <div class='form-control-plaintext'><?= $locals['profile_form_error'] ?></div>
            </div>
        <?php } ?>

        <!-- Name information -->
        <div class='form-row'>

            <!-- Title -->
            <div class='form-group col-md-3'>

                <!-- Header -->
                <label for='title_id'>Title:</label>
                <select class='form-control' id='title_id' name='title_id'>

                    <!-- Output user title (if present), drawn from the database -->
                    <?php if ($locals['user']['title_id'] && $locals['user']['title_name']) { ?>
                        <option value='<?= $locals['user']['title_id'] ?>'><?= $locals['user']['title_name'] ?? '' ?></option>
                    <?php } else { ?>
                        <option value='<?= NULL ?>'>Please Select</option>
                    <?php } ?>

                    <!-- Output list of titles, drawn from the database -->
                    <?php if ($locals['titles']) { ?>
                        <?php foreach ($locals['titles'] as $title) { ?>
                            <option value='<?= $title->get('id') ?? '' ?>'><?= $title->get('name') ?? '' ?></option>
                        <?php } ?>
                    <?php } else { ?>
                        <option value='Error!'></option>
                    <?php }?>
                </select>
            </div>

            <!-- Forename -->
            <div class='form-group col-md'>
                <label for='first_name'>Forename:</label>
                <input type='text' class='form-control' id='first_name' name='first_name' value='<?= $locals['user']['first_name'] ?? '' ?>' required autofocus>
            </div>
            
            <!-- Surname -->
            <div class='form-group col-md'>
                <label for='last_name'>Surname:</label>
                <input type='text' class='form-control' id='last_name' name='last_name' value='<?= $locals['user']['last_name'] ?? '' ?>' required>
            </div>
        </div>

        <!-- Gender & Number -->
        <div class='form-row'>
            
            <!-- Gender -->
            <div class='form-group col-md-5'>
                
                <!-- Header -->
                <label for='gender_id'>Gender:</label>
                <select class='form-control' id='gender_id' name='gender_id'>

                    <!-- Output user gender (if present), drawn from the database -->
                    <?php if ($locals['user']['gender_id'] && $locals['user']['gender_name']) { ?>
                        <option value='<?= $locals['user']['gender_id'] ?? '' ?>'><?= $locals['user']['gender_name'] ?? '' ?></option>
                    <?php } else { ?>
                        <option value='<?= NULL ?>'>Please Select</option>
                    <?php } ?>

                    <!-- Output list of genders, drawn from the database -->
                    <?php if ($locals['genders']) { ?>
                        <?php foreach ($locals['genders'] as $gender) { ?>
                            <option value='<?= $gender->get('id') ?? '' ?>'><?= $gender->get('name') ?? '' ?></option>
                        <?php } ?>
                    <?php } else { ?>
                        <option value='Error!'></option>
                    <?php }?>
                </select>
            </div>

            <!-- Contact number -->
            <div class='form-group col-md-7'>
                <label for='contact_number'>Contact Number:</label>
                <div class='input-group'>
                    <div class='input-group-prepend'>
                        <div class='input-group-text'>+353</div>
                    </div>
                    <input type='text' class='form-control' id='contact_number' name='contact_number' aria-describedby='input_group_prepend' value='<?= $locals['user']['contact_number'] ?? '' ?>'>
                </div>
                <small class='form-text text-right'>Format: 08X-XXX-XXXX</small>
            </div>
        </div>
        
        <!-- Email address -->
        <div class='form-group'>
            <label for='email_address'>Email Address:</label>
            <input type='email' class='form-control' id='email_address' name='email_address' value='<?= $locals['user']['email_address'] ?? '' ?>' required>
        </div>

        <!-- Address Line -->
        <div class='form-row'>

            <!-- Address line 1 -->
            <div class='form-group col-md-6'>
                <label for='address_line_1'>Address Line 1:</label>
                <input type='text' class='form-control' id='address_line_1' name='address_line_1' value='<?= $locals['user']['address_line_1'] ?? '' ?>' maxlength='150'>
            </div>

            <!-- Address line 2 -->
            <div class='form-group col-md-6'>
                <label for='address_line_2'>Address Line 2:</label>
                <input type='text' class='form-control' id='address_line_2' name='address_line_2' value='<?= $locals['user']['address_line_2'] ?? ''?>' maxlength='150'>
            </div>
        </div>

        <!-- Town/city -->
        <div class='form-group'>
            <label for='town_city'>Town/City:</label>
            <input type='text' class='form-control' id='town_city' name='town_city' value='<?= $locals['user']['town_city'] ?? '' ?>' maxlength='150'>
        </div>

        <!-- County & Country -->
        <div class='form-row'>

            <!-- County -->
            <div class='form-group col-md-6'>
                <label for='county'>County:</label>
                <input type='text' class='form-control' id='county' name='county' value='<?= $locals['user']['county'] ?? '' ?>'>
            </div>

            <!-- Country -->
            <div class='form-group col-md-6'>
                <label for='country'>Country:</label>
                <input type='text' class='form-control' id='country' name='country' value='<?= $locals['user']['country'] ?? '' ?>'>
            </div>
        </div>

        <!-- Submit form -->
        <button type='submit' class='btn btn-primary'>Save Changes</button>
    </form>
</div>