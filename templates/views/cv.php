<!-- CV Introduction -->
<div class='card rounded mt-2 bg-light'>
    <div class='card-header'>
        <h5>Introduction</h5>
    </div>
    <?php if (!empty($locals['cv']['user_cv'])) { ?>
        <div class='card-body'>
            <div class='list-group-item'><?= $locals['cv']['user_cv']['introduction'] ?></div>
        </div>
    <?php } else { ?>
        <div class='list-group-item'>No Introduction Found.</div>
        <div class='card-footer'>
            <a class='btn btn-primary' href='add_cv?type=introduction'>Add New</a>
        </div>
    <?php } ?>
</div>

<!-- User Skills -->
<div class='card rounded mt-2 bg-light'>
    <div class='card-header'>
        <h5>Key Skills</h5>
    </div>
    <?php if (!empty($locals['cv']['user_skills'])) { ?>
        <div class='card-body'>
            <?php foreach ($locals['cv']['user_skills'] as $user_skill) { ?>
                <div class='list-group-item clearfix'>
                    <h6 class='float-left pt-2'><?= $user_skill['name'] ?></h6>
                    <a class='btn btn-danger float-right' href='processcv?delete=skill&id=<?= $user_skill['id'] ?>'>Delete</a>
                </div>
            <?php } ?>
        </div>
    <?php } else { ?>
        <div class='list-group-item'>No Key Skills Found.</div>
    <?php } ?>
    <div class='card-footer'>
        <a class='btn btn-primary' href='add_cv?type=skill'>Add New</a>
    </div>
</div>

<!-- User Education -->
<div class='card rounded mt-2 bg-light'>
    <div class='card-header'>
        <h5>Educational history</h5>
    </div>
    <?php if (!empty($locals['cv']['user_educations'])) { ?> 
        <div class='card-body'>
            <?php foreach ($locals['cv']['user_educations'] as $user_education) { ?>
                <div class='list-group-item'>
                    <p><strong>Title: </strong><?= $user_education['title'] ?></p>
                    <p><strong>Level: </strong><?= $user_education['level'] ?></p>
                    <p><strong>Awarding Body: </strong><?= $user_education['awarding_body'] ?></p>
                    <p><strong>Year Recieved: </strong><?= $user_education['year_received'] ?></p>
                    <a class='btn btn-danger' href='processcv?delete=education&id=<?= $user_education['id'] ?>'>Delete</a>
                </div>
            <?php } ?>
        </div>
    <?php } else { ?>
        <div class='list-group-item'>No Educational History Found.</div>
    <?php } ?>
    <div class='card-footer'>
        <a class='btn btn-primary' href='add_cv?type=education'>Add New</a>
    </div>
</div>

<!-- User Experience -->
<div class='card rounded mt-2 bg-light'>
    <div class='card-header'>
        <h5>Professional experiences</h5>
    </div>
    <?php if (!empty($locals['cv']['user_experiences'])) { ?> 
        <div class="card-body">
            <?php foreach ($locals['cv']['user_experiences'] as $user_experience) { ?>
                <div class='list-group-item'>
                    <p><strong>Term: </strong><?= $user_experience['term'] ?></p>
                    <p><strong>Position: </strong><?= $user_experience['position'] ?></p>
                    <p><strong>Company Name: </strong><?= $user_experience['company_name'] ?></p>
                    <p><strong>Job Description: </strong></p>
                    <p><?= $user_experience['description'] ?></p>
                    <a class='btn btn-danger' href='processcv?delete=experience&id=<?= $user_experience['id'] ?>'>Delete</a>
                </div>
            <?php } ?>
        </div>
    <?php } else { ?>
        <div class='list-group-item'>No Experience History Found.</div>
    <?php } ?>
    <div class='card-footer'>
        <a class='btn btn-primary' href='add_cv?type=experience'>Add New</a>
    </div>
</div>

<!-- User Referees -->
<div class='card rounded mt-2 bg-light'>
    <div class='card-header'>
        <h5>References</h5>
    </div>
    <?php if (!empty($locals['cv']['user_referees'])) { ?> 
        <div class="card-body">
            <?php foreach ($locals['cv']['user_referees'] as $user_referee) { ?>
                <div class='list-group-item'>
                    <p><strong>Name: </strong><?= $user_referee['name'] ?></p>
                    <p><strong>Email Address: </strong><?= $user_referee['email_address'] ?></p>
                    <p><strong>Contact Number: </strong><?= $user_referee['contact_number'] ?></p>
                    <p><strong>Description: </strong></p>
                    <p><?= $user_referee['description'] ?></p>
                    <a class='btn btn-danger' href='processcv?delete=referee&id=<?= $user_referee['id'] ?>'>Delete</a>
                </div>
            <?php } ?>
        </div>
    <?php } else { ?>
        <div class='list-group-item'>Delighted to supply on request.</div>
    <?php } ?>
    <div class='card-footer'>
        <a class='btn btn-primary' href='add_cv?type=referee'>Add New</a>
    </div>
</div>

<?php if (!empty($locals['cv'])) { ?>
    <div class='mt-2 mb-2 float-right'><a class='btn btn-info' href='cv?edit=1'>Edit CV</a></div>
<?php } ?>