<form action='add_cv' method='post'>
    <h1>Educational history</h1>
    <div class='mainContentWindow__rowContainer'>
        <div class='form-group col-md-6'>
            <p><strong><label for='educationTitle'>Title: <label></strong><input type='text' class='form-control' name='educationTitle' required></p>
        </div>
        <div class='form-group col-md-6'>
            <p><strong><label for='educationLevel'>Level: <label></strong><input type='text' class='form-control' name='educationLevel' required></p>
        </div>
        <div class='form-group col-md-6'>
            <p><strong><label for='educationAwardBody'>Awarding Body: <label></strong><input type='text' class='form-control' name='educationAwardBody' required></p>
        </div>
        <div class='form-group col-md-6'>
            <p><strong><label for='educationYear'>Year Received: <label></strong><input type='text' class='form-control' name='educationYear' required></p>
        </div>
        <div class='form-group col-md-6'>
            <input type='submit' class='btn btn-primary' value='Add'>
        </div>
    </div>
</form>