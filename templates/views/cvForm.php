<form action='processcv' class='mt-3' method='post'>
    
    <!-- Introduction -->
    <?php if (!empty($locals['cv']['user_cv'])) { ?>
        <h1>Introduction</h1>
        <div class='mainContentWindow__rowContainer'>
            <div class='form-group col-md-6'>  
                <p><strong><label for='introduction'>Introduction text: <label></strong></p>
                <p><textarea class='form-control' name='introduction' rows='10' cols='100'><?= $locals['cv']['user_cv']['introduction'] ?></textarea></p>
            </div>
        </div>
    <?php } ?>

    <!-- User Skills -->
    <?php if (!empty($locals['cv']['user_skills'])) { ?> 
        <h1>Key Skills</h1>
        <div class='mainContentWindow__rowContainer'>
            <?php $count = 0; ?>
            <?php foreach ($locals['cv']['user_skills'] as $user_skill) { ?>
                <div class='form-group col-md-6'>  
                    <p><strong><label for='skill_<?= $count ?>'>Skill <?= ($count + 1) ?>: <label></strong></p>
                    <p><input type='hidden' id='skill_<?= $count ?>_id' name='skill_<?= $count ?>_id' value='<?= $user_skill['id'] ?>'></p>
                    <p><input class='form-control' type='text' name='skill_<?= $count ?>' value='<?= $user_skill['name'] ?>'></p>
                </div>
            <?php $count++ ?>
            <?php } ?>
            <input type='hidden' name='user_skills_count' id='user_skills_count' value=<?= $count ?>>
        </div>
    <?php } ?>
    
    <!-- User Education -->
    <?php if (!empty($locals['cv']['user_educations'])) { ?> 
        <h1>Educational history</h1>
        <div class='mainContentWindow__rowContainer'>
            <?php $count = 0; ?>
            <?php foreach ($locals['cv']['user_educations'] as $user_education) { ?>
                <p><input type='hidden' id='education_<?= $count ?>_id' name='education_<?= $count ?>_id' value='<?= $user_education['id'] ?>'></p>
                <div class='form-group col-md-6'>  
                    <p><strong><label for='educationLevel_<?= $count ?>'>Level: <label></strong><input class='form-control' type='text' name='educationLevel_<?= $count ?>' value='<?= $user_education['level'] ?>'></p>
                </div>
                <div class='form-group col-md-6'>  
                    <p><strong><label for='educationTitle_<?= $count ?>'>Title: <label></strong><input class='form-control' type='text' name='educationTitle_<?= $count ?>' value='<?= $user_education['title'] ?>'></p>
                </div>
                <div class='form-group col-md-6'>  
                    <p><strong><label for='educationAwardBody_<?= $count ?>'>Awarding Body: <label></strong><input class='form-control' type='text' name='educationAwardBody_<?= $count ?>' value='<?= $user_education['awarding_body'] ?>'></p>
                </div>
                <div class='form-group col-md-6'>  
                    <p><strong><label for='educationYear_<?= $count ?>'>Year Received: <label></strong><input class='form-control' type='text' name='educationYear_<?= $count ?>' value='<?= $user_education['year_received'] ?>'></p>
                </div>
            <?php $count++; ?>
            <?php } ?>
            <input type='hidden' name='user_education_count' id='user_education_count' value=<?= $count ?>>
        </div>
    <?php } ?>
    
    <!-- User Experience -->
    <?php if (!empty($locals['cv']['user_experiences'])) { ?>
        <h1>Professional experiences</h1>
        <div class='mainContentWindow__rowContainer'>
            <?php $count = 0; ?>
            <?php foreach ($locals['cv']['user_experiences'] as $user_experience) { ?>
                <p><strong><label for='skill_<?= $count ?>'>Education <?= ($count + 1) ?>: <label></strong></p>
                <p><input type='hidden' id='experience_<?= $count ?>_id' name='experience_<?= $count ?>_id' value='<?= $user_experience['id'] ?>'></p>
                <div class='form-group col-md-6'>  
                    <p><strong><label for='experienceTerm_<?= $count ?>'>Term: <label></strong></p>
                    <p><input class='form-control' type='text' name='experienceTerm_<?= $count ?>' value='<?= $user_experience['term'] ?>'></p>
                </div>
                <div class='form-group col-md-6'>  
                    <p><strong><label for='experiencePosition_<?= $count ?>'>Position: <label></strong></p>
                    <p><input class='form-control' type='text' name='experiencePosition_<?= $count ?>' value='<?= $user_experience['position'] ?>'></p>
                </div>
                <div class='form-group col-md-6'>  
                    <p><strong><label for='experienceCompany_<?= $count ?>'>Company Name: <label></strong></p>
                    <p><input class='form-control' type='text' name='experienceCompany_<?= $count ?>' value='<?= $user_experience['company_name'] ?>'></p>
                </div>
                <div class='form-group col-md'>  
                    <p><strong><label for='experienceDescription_<?= $count ?>'>Job Description: <label></strong></p>
                    <textarea class='form-control' name='experienceDescription_<?= $count ?>' rows='10' cols='100' ><?= $user_experience['description'] ?></textarea>
                </div>
            <?php $count++; ?>
            <?php } ?>
            <input type='hidden' name='user_experience_count' id='user_experience_count' value=<?= $count ?>>
        </div>
    <?php } ?>

    <!-- User Referees -->
    <?php if (!empty($locals['cv']['user_referees'])) { ?> 
        <h1>References</h1>
        <div class='mainContentWindow__rowContainer'>
            <?php $count = 0; ?>
            <?php foreach ($locals['cv']['user_referees'] as $user_referee) { ?>  
                <h5>Referee <?= $count + 1; ?></h5>   
                <p><input type='hidden' id='referee_<?= $count ?>_id' name='referee_<?= $count ?>_id' value='<?= $user_referee['id'] ?>'></p>
                <div class='form-group col-md-6'>  
                    <p><strong><label for='refereeName_<?= $count ?>'>Name: <label></strong></p>
                    <p><input class='form-control' type='text' name='refereeName_<?= $count ?>' value='<?= $user_referee['name'] ?>'></p>
                </div>
                <div class='form-group col-md-6'>  
                    <p><strong><label for='refereeContact_<?= $count ?>'>Contact Number: <label></strong></p>
                    <p><input class='form-control' type='text' name='refereeContact_<?= $count ?>' value='<?= $user_referee['contact_number'] ?>'></p>
                </div>
                <div class='form-group col-md-6'>  
                    <p><strong><label for='refereeEmail_<?= $count ?>'>E-mail address: <label></strong></p>
                    <p><input class='form-control' type='text' name='refereeEmail_<?= $count ?>' value='<?= $user_referee['email_address'] ?>'></p>
                </div>
                <div class='form-group col-md'>  
                    <p><strong><label for='refereeDescription_<?= $count ?>'>Description: <label></strong></p>
                    <textarea class='form-control' name='refereeDescription_<?= $count ?>' rows='10' cols='100' ><?= $user_referee['description'] ?></textarea>
                </div>
            <?php $count++; ?>
            <?php } ?>
            <input type='hidden' name='user_referees_count' id='user_referees_count' value=<?= $count ?>>
        </div>    
    <?php } ?>
    
    <input class='btn btn-primary' type='submit' value='Submit Changes'>
</form>