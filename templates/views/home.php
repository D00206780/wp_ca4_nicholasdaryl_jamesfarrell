<!-- Store posted form for retreival within javascript -->
<input type='hidden' id='posted_form' value='<?= $locals['posted_form'] ?>'>

<!-- Login Form -->
<div class='modal fade' id='modal_login_window' tabindex='-1' role='dialog' aria-labelledby='modal_login_header' aria-hidden='true'>
    
    <!-- Dialog-->
    <div class='modal-dialog' role='document'>

        <!-- Content -->
        <div class='modal-content'>

            <!-- Header -->
            <div class='modal-header'>
                <h5 class='modal-title' id='modal_login_header'>Login</h5>
                <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                    <span aria-hidden='true'>&times;</span>
                </button>
            </div>

            <!-- Body -->
            <div class='modal-body'>
                        
                <!-- Errors -->
                <div class='form-control-plaintext'>
                    <h6 id='login_form_errors'><?= $locals['modal_form_error'] ?? '' ?></h6>
                </div>
                
                <!-- Form -->
                <form action='login' id='modal_login_form' name='modal_login_form' method='POST'>

                    <!-- Email -->
                    <div class='form-group'>
                        <label for='login_email_address'>Email Address:</label>
                        <input type='email' class='form-control' id='login_email_address' name='login_email_address' placeholder='john@doe.com' required>
                    </div>    

                    <!-- Password -->
                    <div class='form-group'>
                        <label for='login_password'>Password:</label>
                        <input type='password' class='form-control' id='login_password' name='login_password' placeholder='Monkeys!' required>
                    </div>

                    <!-- Submit -->
                    <button type='submit' class='btn btn-primary'>Login</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Register Form -->
<div class='modal fade' id='modal_register_window' tabindex='-1' role='dialog' aria-labelledby='modal_register_header' aria-hidden='true'>
    
    <!-- Dialog-->
    <div class='modal-dialog' role='document'>

        <!-- Content -->
        <div class='modal-content'>

            <!-- Header -->
            <div class='modal-header'>
                <h5 class='modal-title' id='modal_register_header'>Register</h5>
                <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                    <span aria-hidden='true'>&times;</span>
                </button>
            </div>
            
            <!-- Body -->
            <div class='modal-body'>
                        
                <!-- Errors -->
                <div class='form-control-plaintext'>
                    <h6 id='register_form_errors'><?= $locals['modal_form_error'] ?? '' ?></h6>
                </div>
                            
                <!-- Form -->
                <form action='register' class='needs-validation' id='modal_register_form' name='modal_register_form' method='POST' novalidate>
                    
                    <!-- Name Row -->
                    <div class='form-row'>

                        <!-- Forename -->
                        <div class='form-group col-md-6'>
                            <label for='register_first_name'>Forename:</label>
                            <input type='text' class='form-control' id='register_first_name' name='register_first_name' placeholder='John' required autofocus>
                            <div class='invalid-feedback' id='register_first_name_feedback'></div>
                            <div class='valid-feedback'> </div>
                        </div>    

                        <!-- Surname -->
                        <div class='form-group col-md-6'>
                            <label for='register_last_name'>Surname:</label>
                            <input type='text' class='form-control' id='register_last_name' name='register_last_name' placeholder='Doe' required>
                            <div class='invalid-feedback' id='register_last_name_feedback'></div>
                            <div class='valid-feedback'></div>
                        </div>  
                    </div>
                    
                    <!-- Email -->
                    <div class='form-group'>
                        <label for='register_email_address'>Email Address:</label>
                        <input type='email' class='form-control' id='register_email_address' name='register_email_address' placeholder='john@doe.com' autocomplete="off" required>
                        <div class='invalid-feedback' id='register_email_address_feedback'></div>
                        <div class='valid-feedback'>Looks good!</div>
                    </div>  

                    <!-- Password Row -->
                    <div class='form-row'>

                        <!-- Password -->
                        <div class='form-group col-md-6'>
                            <label for='register_password'>Password:</label>
                            <input type='password' class='form-control' id='register_password' name='register_password' placeholder='Monkeys!' required>
                            <div class='invalid-feedback'  id='register_password_feedback'></div>
                            <div class='valid-feedback'>Looks good!</div>
                        </div>

                        <!-- Confirmation -->
                        <div class='form-group col-md-6'>
                            <label for='register_confirm_password'>Confirm Password:</label>
                            <input type='password' class='form-control' id='register_confirm_password' name='register_confirm_password' placeholder='Monkeys!' autocomplete='off' required>
                            <div class='invalid-feedback' id='register_confirm_password_feedback'></div>
                            <div class='valid-feedback'></div>
                        </div>
                    </div>
                    
                    <!-- Account type -->
                    <div class='form-group'>                            
                        <label for='register_account_type'>Account type:</label>
                        <select class='custom-select is-invalid' name='register_account_type' id='register_account_type' required>
                            <option value=''>Pick one</option>
                            <option value='0'>Job Seeker</option>
                            <option value='1'>Employer</option>
                        </select>
                        <div class='invalid-feedback'>Please select!</div>
                        <div class='valid-feedback'></div>
                    </div>

                    <!-- Submit -->
                    <button type='submit' class='btn btn-primary'>Register</button>
                </form>
            </div>
        </div>
	</div>
</div>