<form action='add_cv' method='post'>
    <h1>Add Reference</h1>
    <div class='mainContentWindow__rowContainer'>
        <div class='form-group col-md-6'>
            <strong><label for='refereeName'>Name: <label></strong>
            <input type='text' class='form-control' name='refereeName' required>
        </div>
        <div class='form-group col-md-6'>
            <strong><label for='refereeEmail'>E-mail address: <label></strong>
            <input type='text' class='form-control' name='refereeEmail' required>
        </div>
        <div class='form-group col-md-6'>
            <strong><label for='refereeContact'>Contact Number: <label></strong>
            <input type='text' class='form-control' name='refereeContact' required>
        </div>
        <div class='form-group col-md'>
            <p><strong><label for='refereeDescription'>Description: <label></strong></p>
            <textarea name='refereeDescription' class='form-control' rows='10' cols='100' required></textarea>
        </div>
        <div class='form-group col-md'>
            <p><input type='submit' class='btn btn-primary' value='Add'></p>
        </div>
    </div>
</form>