<div class='container'>
    <?php $job = $locals['job'] ?>

    <div class='card mt-3 p-0'>
        <img src='assets/images/card.png' class='card-img-top' alt='...'>
        <div class='row shadow-sm no-gutters'>
            <div class='card col-md-3'>
                <div class='card-header'>
                    <img src='assets/images/job.png' class='col-md-10 offset-md-1' alt='...'>
                </div>
                <div class='card-body text-center'>
                    <div class='card-text'><?= $job->get('title') ?> </div>
                    <div class='card-text'><?= $job->get('company') ?></div>
                    <div class='card-text'><?= $job->get('term') ?></div>
                    <div class='card-text'><?= $job->get('location') ?></div>
                    <div class='card-text'><?= $job->get('rate') ?></div>
                </div>
            </div>
            <div class='card col-md-8'>
                <div class='card-body text-center'>
                    <div class='card-text'><?= $job->get('description') ?></div>
                </div>
                <div class='card-footer text-center'>
                    <?php if (!$locals['account_type']) { ?>
                        <a class='btn col-md-2 bg-primary text-white' href='apply_job?job_id=<?= $job->get('id') ?>'><small>Apply</small></a>
                    <?php } ?>
                </div>
            </div>
            <div class='col-md-1 text-white bg-secondary'></div>
        </div>
    </div>


</div>
