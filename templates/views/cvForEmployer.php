<h1>Introduction</h1>
<?php if (!empty($locals['cv']['user_cv'])) { ?>
    <div class='mainContentWindow__rowContainer'>
        <p><?= $locals['cv']['user_cv']['introduction'] ?></p>
    </div>
<?php } else { ?>
    <p>No Introduction Found.</p>
<?php } ?>

<h1>Key Skills</h1>
<?php if (!empty($locals['cv']['user_skills'])) { ?>
    <div class='mainContentWindow__rowContainer'>
        <?php foreach ($locals['cv']['user_skills'] as $user_skill) { ?>
            <h3><?= $user_skill['name'] ?></h3>
        <?php } ?>
    </div>
<?php } else { ?>
    <p>No Key Skills Found.</p>
<?php } ?>

<h1>Educational history</h1>
<?php if (!empty($locals['cv']['user_educations'])) { ?> 
    <?php foreach ($locals['cv']['user_educations'] as $user_education) { ?>
        <div class='mainContentWindow__rowContainer'>
            <p><strong>Title: </strong><?= $user_education['title'] ?></p>
            <p><strong>Level: </strong><?= $user_education['level'] ?></p>
            <p><strong>Awarding Body: </strong><?= $user_education['awarding_body'] ?></p>
            <p><strong>Year Recieved: </strong><?= $user_education['year_received'] ?></p>
        </div>
    <?php } ?>
<?php } else { ?>
    <p>No Educational History Found.</p>
<?php } ?>

<h1>Proffesional experiences</h1>
<?php if (!empty($locals['cv']['user_experiences'])) { ?> 
    <?php foreach ($locals['cv']['user_experiences'] as $user_experience) { ?>
        <div class='mainContentWindow__rowContainer'>
            <p><strong>Term: </strong><?= $user_experience['term'] ?></p>
            <p><strong>Position: </strong><?= $user_experience['position'] ?></p>
            <p><strong>Company Name: </strong><?= $user_experience['company_name'] ?></p>
            <p><strong>Job Description: </strong></p>
            <p><?= $user_experience['description'] ?></p>
        </div>
    <?php } ?>
<?php } else { ?>
    <p>No Experience History Found.</p>
<?php } ?>

<h1>References</h1>
<?php if (!empty($locals['cv']['user_referees'])) { ?> 
    <?php foreach ($locals['cv']['user_referees'] as $user_referee) { ?>
        <div class='mainContentWindow__rowContainer'>
            <p><strong>Name: </strong><?= $user_referee['name'] ?></p>
            <p><strong>Email Address: </strong><?= $user_referee['email_address'] ?></p>
            <p><strong>Contact Number: </strong><?= $user_referee['contact_number'] ?></p>
            <p><strong>Description: </strong></p>
            <p><?= $user_referee['description'] ?></p>
        </div>
    <?php } ?>
<?php } else { ?>
    <p>Delighted to supply on request.</p>
<?php } ?>