<?php require_once('models/Model.php'); ?>

<?php 

    class UserGender {

        private $id;
        private $name;

        public function __construct($properties) {

            if (!is_array($properties)) {
                throw new Exception('Invalid properties array passed to UserGender constructor');
            }

            $this->setId($properties['id']      ?? NULL);
            $this->setName($properties['name']  ?? NULL);
        }


        /** */
        #################################################
        # Static Functions
        #################################################
        /** */

        public static function getAllGenders(PDO $app_db_connection) {

            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for UserGender getAllGenders');
            }
            
            $select = $app_db_connection->prepare('SELECT * FROM user_genders;');
            $select->execute();

            return array_map(function($row) {
                return new UserGender($row);
            }, $select->fetchAll());
        }

        public static function getGenderById(PDO $app_db_connection, $id) {
            
            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for UserGender getGenderById');
            }
            
            if (!$id) return; 
        
            $id = (int) $id;
        
            if (!Model::isValidId($id)) {
                throw new Exception('ID for getGenderById must be positive numeric');
            }

            $select = $app_db_connection->prepare('SELECT * FROM user_genders WHERE id = :id LIMIT 1;');
            $select->bindParam(':id', $id);
            $select->execute();

            $row = $select->fetch();
            
            return $row !== FALSE
                ? new UserGender($row)
                : NULL;
        }


        /** */
        #################################################
        # Validation Functions
        #################################################
        /** */

        public static function checkGenderIdValid(PDO $app_db_connection, $gender_id) {

            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for UserGender checkGenderIdValid');
            }

            return Model::checkIfExists($app_db_connection, 'user_gender', 'gender_id', $gender_id);
        }

        /** */
        #################################################
        # Getters
        #################################################
        /** */

        public function get($column_name) {
            return $this->$column_name;
        }


        /** */
        #################################################
        # Setters
        #################################################
        /** */

        public function setId($id) {

            if ($id === NULL) {
                $this->id = NULL;
                return;
            }

            if (!Model::isValidId($id)) {
                throw new Exception('ID for setId must be positive numeric');
            }

            $this->id = $id;
        }
        
        public function setName($name) {

            if ($name === NULL) return;

            if (!Model::isValidName($name)) {
                throw new Exception('Gender must be between 2 and 55 characters long');
            }

            $this->name = $name;
        }
    }
?>