<?php 

    class JobCategory {

        private $id;
        private $name;

        public function __construct($properties) {

            if (!is_array($properties)) {
                throw new Exception('Invalid properties array passed to JobCategory constructor');
            }

            $this->setId($properties['id']      ?? NULL);
            $this->setName($properties['name']  ?? NULL);
        }


        /** */
        #################################################
        # Static Functions
        #################################################
        /** */

        public static function getAllCategories(PDO $app_db_connection) {

            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for JobCategory getAllCategories');
            }
            
            $select = $app_db_connection->prepare('SELECT * FROM job_categories;');
            $select->execute();

            return array_map(function($row) {
                return new JobCategory($row);
            }, $select->fetchAll());
        }

        public static function getCategoryById(PDO $app_db_connection, $id) {
            
            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for JobCategory getCategoryById');
            }
        
            $id = (int) $id;
        
            if (!Model::isValidId($id)) {
                throw new Exception('ID for getCategoryById must be positive numeric');
            }

            $select = $app_db_connection->prepare('SELECT * FROM job_categories WHERE id = :id LIMIT 1;');
            $select->bindParam(':id', $id);
            $select->execute();

            $row = $select->fetch();
            
            return $row !== FALSE
                ? new JobCategory($row)
                : NULL;
        }


        /** */
        #################################################
        # Validation Functions
        #################################################
        /** */

        public static function checkCategoryIdValid(PDO $app_db_connection, $category_id) {

            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for JobCategory checkCategoryIdValid');
            }

            return Model::checkIfExists($app_db_connection, 'job_category', 'category_id', $category_id);
        }

        /** */
        #################################################
        # Getters
        #################################################
        /** */

        public function get($column_name) {
            return $this->$column_name;
        }


        /** */
        #################################################
        # Setters
        #################################################
        /** */

        public function setId($id) {

            if ($id === NULL) {
                $this->id = NULL;
                return;
            }

            if (!Model::isValidId($id)) {
                throw new Exception('ID for setId must be positive numeric');
            }

            $this->id = $id;
        }
        
        public function setName($name) {

            if ($name === NULL) return;

            if (!Model::isValidString($name)) {
                throw new Exception('Category must be between 2 and 55 characters long');
            }

            $this->name = $name;
        }

    }

?>