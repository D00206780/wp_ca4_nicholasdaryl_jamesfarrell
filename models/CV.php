<?php 

    class CV {

        private $id;
        private $introduction;
        private $user_id;

        public function __construct($properties) {

            if (!is_array($properties)) {
                throw new Exception('Invalid properties array passed to CV constructor');
            }

            $this->setId($properties['id'])                     ?? NULL;
            $this->setIntroduction($properties['introduction']) ?? NULL;
            $this->setUserId($properties['user_id'])            ?? NULL;

        }


        /** */
        #################################################
        # Instance Methods
        #################################################
        /** */

        public function save(PDO $app_db_connection) {

            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for CV save');
            }

            if(!$this->get('id')) {
        
                $insert = $app_db_connection->prepare('INSERT INTO user_cv (introduction, user_id) VALUES (:introduction, :user_id);');
                $insert->execute([
                    'introduction' => $this->get('introduction') ?? NULL,
                    'user_id'      => $this->get('user_id')     ?? NULL,
                ]);

                $saved = $insert->rowCount() === 1;
            
                if ($saved) {
                    $this->setId($app_db_connection->lastInsertId());
                }

                return $saved;

            } else {

                $update = $app_db_connection->prepare('UPDATE user_cv SET introduction = :introduction, user_id = :user_id WHERE id = :id;');
                $update->execute([
                    'id'           => $this->get('id')           ?? NULL,
                    'introduction' => $this->get('introduction') ?? NULL,
                    'user_id'      => $this->get('user_id')      ?? NULL,
                ]);

                return $update->rowCount() === 1;
            }

        }

        public function delete(PDO $app_db_connection) {

            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for CV delete');
            }

            if ($this->get('id') === NULL) {
                throw new Exception('Cannot delete a transient CV object');
            }
        
            $delete = $app_db_connection->prepare('DELETE FROM cv WHERE id = :id;');
            $delete->bindParam(':id', $id);
            $delete->execute();
        
            $deleted = $delete->rowCount() === 1;
        
            if ($deleted) {
              $this->setId(NULL);
            }
        
            return $deleted;
        }


        /** */
        #################################################
        # Static Functions
        #################################################
        /** */

        public static function getCVByUserId(PDO $app_db_connection, $user_id) {

            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for CV getAllExperienceByUserId');
            }
        
            $user_id = (int) $user_id;
        
            if (!Model::isValidId($user_id)) {
                throw new Exception('ID for getCVByUserId must be positive numeric');
            }

            $select = $app_db_connection->prepare('SELECT * FROM user_cv WHERE user_id = :user_id LIMIT 1;');
            $select->bindParam(':user_id', $user_id);
            $select->execute();

            return array_map(function($row) {
                return new CV($row);
            }, $select->fetchAll());

        }


        /** */
        #################################################
        # Getters
        #################################################
        /** */
        
        public function get($column_name) {
            return $this->$column_name;
        }


        /** */
        #################################################
        # Setters
        #################################################
        /** */

        public function setId($id) {

            if (!$id) {
                $this->id = NULL;
                return;
            }

            if (!Model::isValidId($id)) {
                throw new Exception('Invalid job id!');
            }

            $this->id = $id;
        }

        public function setIntroduction($introduction) {

            if ($introduction === NULL) {
                return;
            }

            if (!Model::isValidString($introduction)) {
                throw new Exception('Invalid introduction entered!');
            }

            $this->introduction = $introduction;

        }

        
        public static function getCVArray(PDO $app_db_connection, $user_id) {
            
            $result = [];

            // get the user_cv of the user and push to result array
            $select = $app_db_connection->prepare('SELECT * FROM user_cv WHERE user_id = :user_id;');
            $select->bindParam(':user_id', $user_id);
            $select->execute();
            $result['user_cv'] = $select->fetch();

            // get the user_education of the user and push to result array
            $select = $app_db_connection->prepare('SELECT * FROM user_education WHERE user_id = :user_id;');
            $select->bindParam(':user_id', $user_id);
            $select->execute();
            $result['user_educations'] = $select->fetchAll();

            // get the user_experience of the user and push to result array
            $select = $app_db_connection->prepare('SELECT * FROM user_experience WHERE user_id = :user_id;');
            $select->bindParam(':user_id', $user_id);
            $select->execute();
            $result['user_experiences'] = $select->fetchAll();

            // get the user_referees of the user and push to result array
            $select = $app_db_connection->prepare('SELECT * FROM user_referees WHERE user_id = :user_id;');
            $select->bindParam(':user_id', $user_id);
            $select->execute();
            $result['user_referees'] = $select->fetchAll();

            // get the user_skills of the user and push to result array
            $select = $app_db_connection->prepare('SELECT * FROM user_skills WHERE user_id = :user_id;');
            $select->bindParam(':user_id', $user_id);
            $select->execute();
            $result['user_skills'] = $select->fetchAll();


            return $result;
        }

        public function setUserId($user_id) {

            if ($user_id === NULL) {
                return;
            }

            if (!Model::isValidId($user_id)) {
                throw new Exception('ID for setUserId must be positive numeric');
            }

            $this->user_id = $user_id;
        }

    }

// public static function createUserCV(PDO $app_db_connection, $properties) {
//     $insert = $app_db_connection->prepare('INSERT INTO user_cv (introduction, user_id) VALUES (:introduction, :user_id)');
//     $insert->bindParam('introduction', $properties['introduction']);
//     $insert->bindParam('user_id', $properties['user_id']);
//     $insert->execute();
// }

// public static function createUserExperience(PDO $app_db_connection, $properties) {
//     $insert = $app_db_connection->prepare('INSERT INTO user_experience (position, company_name, term, description, user_id) VALUES (:position, :company_name, :term, :description, :user_id)');
//     $insert->bindParam('position',     $properties['position']);
//     $insert->bindParam('company_name', $properties['company_name']);
//     $insert->bindParam('term',         $properties['term']);
//     $insert->bindParam('description',  $properties['description']);
//     $insert->bindParam('user_id',      $properties['user_id']);
//     $insert->execute();
// }

// public static function createUserEducation(PDO $app_db_connection, $properties) {
//     $insert = $app_db_connection->prepare('INSERT INTO user_education (level, title, awarding_body, year_received, user_id) VALUES (:level, :title, :awarding_body, :year_recieved, :user_id)');
//     $insert->bindParam('level',         $properties['level']);
//     $insert->bindParam('title',         $properties['title']);
//     $insert->bindParam('awarding_body', $properties['awarding_body']);
//     $insert->bindParam('year_recieved', $properties['year_recieved']);
//     $insert->bindParam('user_id',       $properties['user_id']);
//     $insert->execute();
// }

// public static function createUserSkills(PDO $app_db_connection, $properties) {
//     $insert = $app_db_connection->prepare('INSERT INTO user_skills (name, user_id) VALUES (:name, :user_id)');
//     $insert->bindParam('name',           $properties['name']);
//     $insert->bindParam('user_id',        $properties['user_id']);
//     $insert->execute();
// }

// public static function createUserReferee(PDO $app_db_connection, $properties) {
//     $insert = $app_db_connection->prepare('INSERT INTO user_referees (name, email_address, contact_number, description, user_id) VALUES (:name, :email_address, :contact_number, :description, :user_id)');
//     $insert->bindParam('name',           $properties['name']);
//     $insert->bindParam('email_address',  $properties['email_address']);
//     $insert->bindParam('contact_number', $properties['contact_number']);
//     $insert->bindParam('description',    $properties['description']);
//     $insert->bindParam('user_id',        $properties['user_id']);
//     $insert->execute();
// }

// public static function updateCV(PDO $app_db_connection, $properties) {
//     // example of $properties, as an asscociative array
//     /*
//         $properties = [
//             'user_id' => 1,
//             'user_cv' => ['cv_id' => 1, introduction' => 'bl bla bla'],
//             'user_experiences' => [
//                 ['id' => 1, position' => 'bla bla', 'company_name' => 'bla bla', 'term' => 'bla bla', 'description' => 'bla bla'],
//                 ['id' => 2, position' => 'bla bla 2', 'company_name' => 'bla bla 2', 'term' => 'bla bla 2', 'description' => 'bla bla 2']
//             ],
//             'user_educations' => [
//                 ['id' => 1, level' => 'bla bla', 'title' => 'bla bla', 'awarding_body' => 'bla bla', 'year_received' => 'bla bla'],
//                 ['id' => 2, level' => 'bla bla 2', 'title' => 'bla bla 2', 'awarding_body 2' => 'bla bla 2', 'year_received' => 'bla bla 2']
//             ],
//             'user_referees' => [
//                 ['id' => 1, 'name' => 'bla bla', 'email_address' => 'bla bla', 'contact_number' => 'bla bla', 'description' => 'bla bla'],
//                 ['id' => 2, 'name' => 'bla bla 2', 'email_address' => 'bla bla 2', 'contact_number' => '08283839292', 'description' => 'bla bla 2']
//             ],
//             'user_skills' => [
//                 ['id' => 1, 'name' => 'bla bla'],
//                 ['id' => 2, 'name' => 'bla bla 3']
//                 ['id' => 3, 'name' => 'bla bla 2'],
//             ]
//         ]
//     */

//     # Update introduction
//     $update = $app_db_connection->prepare('UPDATE user_cv SET introduction = :introduction WHERE user_id = :user_id');
//     $update->bindParam('user_id',           $properties['user_id']);
//     $update->bindParam('introduction',      $properties['user_cv']['introduction']);
//     $update->execute();

//     # Update experience
//     for ($i = 0; $i < sizeOf($properties['user_experiences']); $i++) {
//         $update = $app_db_connection->prepare('UPDATE user_experience SET position = :position, company_name = :company_name, term = :term, description = :description WHERE id = :id');
//         $update->bindParam('id',                $properties['user_experiences'][$i]['id']);
//         $update->bindParam('position',          $properties['user_experiences'][$i]['position']);
//         $update->bindParam('term',              $properties['user_experiences'][$i]['term']);
//         $update->bindParam('company_name',      $properties['user_experiences'][$i]['company_name']);
//         $update->bindParam('description',       $properties['user_experiences'][$i]['description']);
//         $update->execute();
//     }

//     # Update education
//     for ($i = 0; $i < sizeOf($properties['user_educations']); $i++) {
//         $update = $app_db_connection->prepare('UPDATE user_education SET title = :title, level = :level, awarding_body = :awarding_body, year_received = :year_recieved WHERE id = :id');
//         $update->bindParam('id',                $properties['user_educations'][$i]['id']);
//         $update->bindParam('title',             $properties['user_educations'][$i]['title']);
//         $update->bindParam('awarding_body',     $properties['user_educations'][$i]['awarding_body']);
//         $update->bindParam('level',             $properties['user_educations'][$i]['level']);
//         $update->bindParam('year_recieved',     $properties['user_educations'][$i]['year_recieved']);
//         $update->execute();
//     }

//     # Update references
//     for ($i = 0; $i < sizeOf($properties['user_referees']); $i++) {
//         $update = $app_db_connection->prepare('UPDATE user_referees SET name = :name, email_address = :email_address, contact_number = :contact_number, description = :description WHERE id = :id');
//         $update->bindParam('id',                $properties['user_referees'][$i]['id']);
//         $update->bindParam('name',              $properties['user_referees'][$i]['name']);
//         $update->bindParam('contact_number',    $properties['user_referees'][$i]['contact_number']);
//         $update->bindParam('email_address',     $properties['user_referees'][$i]['email_address']);
//         $update->bindParam('description',       $properties['user_referees'][$i]['description']);
//         $update->execute();
//     }

//     # Update skills
//     for ($i = 0; $i < sizeOf($properties['user_skills']); $i++) {
//         $update = $app_db_connection->prepare('UPDATE user_skills SET name = :name WHERE id = :id');
//         $update->bindParam('id',                $properties['user_skills'][$i]['id']);
//         $update->bindParam('name',              $properties['user_skills'][$i]['name']);
//         $update->execute();
//     }
// }

// public static function deleteUserExperience(PDO $app_db_connection, $id) {
//     $delete = $app_db_connection->prepare('DELETE FROM user_experience WHERE id = :id;');
//     $delete->bindParam(':id', $id);
//     $delete->execute();
// }

// public static function deleteUserEducation(PDO $app_db_connection, $id) {
//     $delete = $app_db_connection->prepare('DELETE FROM user_education WHERE id = :id;');
//     $delete->bindParam(':id', $id);
//     $delete->execute();
// }

// public static function deleteUserReferees(PDO $app_db_connection, $id) {
//     $delete = $app_db_connection->prepare('DELETE FROM user_referees WHERE id = :id;');
//     $delete->bindParam(':id', $id);
//     $delete->execute();
// }

// public static function deleteUserSkills(PDO $app_db_connection, $id) {
//     $delete = $app_db_connection->prepare('DELETE FROM user_skills WHERE id = :id;');
//     $delete->bindParam(':id', $id);
//     $delete->execute();
// }

// public static function deleteUserCv(PDO $app_db_connection, $id) {
//     $delete = $app_db_connection->prepare('DELETE FROM user_cv WHERE id = :id;');
//     $delete->bindParam(':id', $id);
//     $delete->execute();
// }

?>