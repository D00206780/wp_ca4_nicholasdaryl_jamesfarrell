<?php require_once('models/Model.php'); ?>

<?php 

    class Job {

        private $id;
        private $title;
        private $description;
        private $company;
        private $location;
        private $term;
        private $rate;
        private $user_id;
        private $industry_id;
        private $category_id;

        public function __construct($properties) {

            if (!is_array($properties)) {
                throw new Exception('Invalid properties array passed to Job constructor');
            }

            $this->setId($properties['id']                   ?? NULL);
            $this->setTitle($properties['title']             ?? NULL);
            $this->setDescription($properties['description'] ?? NULL);
            $this->setCompany($properties['company']         ?? NULL);
            $this->setLocation($properties['location']       ?? NULL);
            $this->setTerm($properties['term']               ?? NULL);
            $this->setRate($properties['rate']               ?? NULL);
            $this->setUserId($properties['user_id']          ?? NULL);
            $this->setIndustryId($properties['industry_id']  ?? NULL);
            $this->setCategoryId($properties['category_id']  ?? NULL);
        }
    
        /** */
        #################################################
        # Instance Methods
        #################################################
        /** */

        public function save(PDO $app_db_connection) {

            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for Job save');
            }

            if($this->get('id') === NULL) {
        
                $insert = $app_db_connection->prepare('INSERT INTO jobs (title, company, description, user_id, location, term, rate, industry_id, category_id) VALUES (:title, :company, :description, :user_id, :location, :term, :rate, :industry_id, :category_id);');
                $insert->execute([
                    'title'         => $this->get('title'),
                    'company'       => $this->get('company'),
                    'description'   => $this->get('description'),
                    'location'      => $this->get('location'),
                    'term'          => $this->get('term'),
                    'rate'          => $this->get('rate'),
                    'user_id'       => $this->get('user_id'),
                    'industry_id'   => $this->get('industry_id'),
                    'category_id'   => $this->get('category_id'),
                ]);
                
                $saved = $insert->rowCount() === 1;
            
                if ($saved) {
                    $this->setId($app_db_connection->lastInsertId());
                }
            
                return $saved;

            } else {
        
                $update = $app_db_connection->prepare('UPDATE jobs SET title = :title, company = :company, description = :description, location = :location, term = :term, rate = :rate, user_id = :user_id, industry_id = :industry_id, category_id = :category_id WHERE id = :id');
                $update->execute([
                    'id'            => $this->get('id'),
                    'title'         => $this->get('title'),
                    'company'       => $this->get('company'),
                    'description'   => $this->get('description'),
                    'location'      => $this->get('location'),
                    'term'          => $this->get('term'),
                    'rate'          => $this->get('rate'),
                    'user_id'       => $this->get('user_id'),
                    'industry_id'   => $this->get('industry_id'),
                    'category_id'   => $this->get('category_id'),
                ]);
                
                return $update->rowCount() === 1;
            }

        }
    
        public function delete(PDO $app_db_connection) {

            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for Job delete');
            }

            if ($this->get('id') === NULL) {
                throw new Exception('Cannot delete a transient Job object');
            }
        
            $id = $this->get('id');

            $delete = $app_db_connection->prepare('DELETE FROM jobs WHERE id = :id;');
            $delete->execute([
                'id' => $id,
            ]);
        
            $deleted = $delete->rowCount() === 1;
        
            if ($deleted) {
                $this->setId(NULL);
            }

            return $deleted;
        }


        /** */
        #################################################
        # Static Functions
        #################################################
        /** */

        public static function getJobById(PDO $app_db_connection, $id) {
            
            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for Job getJobById');
            }
        
            if (!$id) return;

            $id = (int) $id;
        
            if (!Model::isValidId($id)) {
                throw new Exception('ID for getJobById must be positive numeric');
            }

            $select = $app_db_connection->prepare('SELECT * FROM jobs WHERE id = :id LIMIT 1;');
            $select->bindParam(':id', $id);
            $select->execute();

            $row = $select->fetch();
            
            return $row !== FALSE
                ? new Job($row)
                : NULL;
        }

        public static function getAllJobsByUserId(PDO $app_db_connection, $user_id) {
            
            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for Job getAllJobsByUserId');
            }
        
            if (!$user_id) return;

            $user_id = (int) $user_id;
        
            if (!Model::isValidId($user_id)) {
                throw new Exception('ID for getJobById must be positive numeric');
            }

            $select = $app_db_connection->prepare('SELECT * FROM jobs WHERE user_id = :user_id;');
            $select->bindParam(':user_id', $user_id);
            $select->execute();
            
            return array_map(function($row) {
                return new Job($row);
            }, $select->fetchAll());
        }

        public static function getAllJobs(PDO $app_db_connection) {

            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for Job getAllJobs');
            }

            $select = $app_db_connection->prepare('SELECT * FROM jobs;');
            $select->execute();

            return array_map(function($row) {
                return new Job($row);
            }, $select->fetchAll());
        }


        /** */
        #################################################
        # Getters
        #################################################
        /** */

        public function get($column_name) {
            return $this->$column_name;
        }

        
        /** */
        #################################################
        # Setters
        #################################################
        /** */

        public function setId($id) {

            if (!$id) {
                $this->id = NULL;
                return;
            }

            if (!Model::isValidId($id)) {
                throw new Exception('Invalid job id!');
            }

            $this->id = $id;
        }
        
        public function setTitle($title) {

            if (!$title) {
                throw new Exception('Job title must not be empty!');
            }

            if (!Model::isValidString($title)) {
                throw new Exception('Invalid job title entered!');
            }

            $this->title = $title;
        }

        public function setCompany($company) {

            if (!$company) return;
            
            if (!Model::isValidString($company)) {
                throw new Exception('Invalid company name entered!');
            }

            $this->company = $company;
        }

        public function setDescription($description) {

            if (!$description) return;

            if (!Model::isValidString($description)) {
                throw new Exception('Invalid description entered!');
            }

            $this->description = $description;
        }

        public function setLocation($location) {

            if (!$location) return;

            if (!Model::isValidString($location)) {
                throw new Exception('Invalid location entered!');
            }

            $this->location = $location;
        }

        public function setTerm($term) {

            if (!$term) return;

            $this->term = $term;
        }

        public function setRate($rate) {

            if (!$rate) return;

            $this->rate = $rate;
        }

        public function setUserId($user_id) {

            if (!$user_id) return;

            if (!Model::isValidId($user_id)) {
                throw new Exception('Invalid user id!');
            }

            $this->user_id = $user_id;
        }

        public function setIndustryId($industry_id) {

            if (!$industry_id) return;

            if (!Model::isValidId($industry_id)) {
                throw new Exception('Invalid industry selected!');
            }

            $this->industry_id = $industry_id;
        }

        public function setCategoryId($category_id) {

            if (!$category_id) return;

            if (!Model::isValidId($category_id)) {
                throw new Exception('Invalid category selected!');
            }

            $this->category_id = $category_id;
        }
    }
?>