<?php require_once('models/Model.php'); ?>

<?php 

    class UserEducation {

        private $id;
        private $level;
        private $title;
        private $awarding_body;
        private $year_received;
        private $user_id;

        public function __construct($properties) {
    
            if (!is_array($properties)) {
                throw new Exception('Invalid properties array passed to UserEducation constructor');
            }

            $this->setId($properties['id']                      ?? NULL);
            $this->setLevel($properties['level']                ?? NULL);
            $this->setTitle($properties['title']                ?? NULL);
            $this->setAwardingBody($properties['awarding_body'] ?? NULL);
            $this->setyearReceived($properties['year_received'] ?? NULL);
            $this->setUserId($properties['user_id']             ?? NULL);
        }


        /** */
        #################################################
        # Instance Methods
        #################################################
        /** */

        public function save(PDO $app_db_connection) {

            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for UserEducation save');
            }

            if(!$this->get('id')) {
        
                $insert = $app_db_connection->prepare('INSERT INTO user_education (level, title, awarding_body, year_received, user_id) VALUES (:level, :title, :awarding_body, :year_received, :user_id);');
                $insert->execute([
                    'level'         => $this->get('level')         ?? NULL,
                    'title'         => $this->get('title')         ?? NULL,
                    'awarding_body' => $this->get('awarding_body') ?? NULL,
                    'year_received' => $this->get('year_received') ?? NULL,
                    'user_id'       => $this->get('user_id')       ?? NULL,
                ]);

                $saved = $insert->rowCount() === 1;
            
                if ($saved) {
                    $this->setId($app_db_connection->lastInsertId());
                }

                return $saved;

            } else {

                $update = $app_db_connection->prepare('UPDATE user_education SET level = :level, title = :title, awarding_body = :awarding_body, year_received = :year_received WHERE id = :id;');
                $update->execute([
                    'id'            => $this->get('id')            ?? NULL,
                    'level'         => $this->get('level')         ?? NULL,
                    'title'         => $this->get('title')         ?? NULL,
                    'awarding_body' => $this->get('awarding_body') ?? NULL,
                    'year_received' => $this->get('year_received') ?? NULL,
                ]);

                return $update->rowCount() === 1;
            }

        }

        public function delete(PDO $app_db_connection) {

            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for UserEducation delete');
            }

            if ($this->get('id') === NULL) {
                throw new Exception('Cannot delete a transient UserEducation object');
            }
        
            $delete = $app_db_connection->prepare('DELETE FROM user_education WHERE id = :id;');
            $delete->bindParam(':id', $this->get('id'));
            $delete->execute();
        
            $deleted = $delete->rowCount() === 1;
        
            if ($deleted) {
              $this->setId(NULL);
            }
        
            return $deleted;
        }

        
        /** */
        #################################################
        # Static Functions
        #################################################
        /** */

        public static function getAllEducationByUserId(PDO $app_db_connection, $user_id) {

            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for UserEducation getAllEducationByUserId');
            }
        
            $user_id = (int) $user_id;
        
            if (!Model::isValidId($user_id)) {
                throw new Exception('ID for getAllEducationByUserId must be positive numeric');
            }

            $select = $app_db_connection->prepare('SELECT * FROM user_education WHERE user_id = :user_id;');
            $select->bindParam(':user_id', $user_id);
            $select->execute();

            return array_map(function($row) {
                return new UserEducation($row);
            }, $select->fetchAll());

        }

        public static function getEducationById(PDO $app_db_connection, $id) {
            
            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for UserEducation getEducationById');
            }
            
            if (!$id) return; 
        
            $id = (int) $id;
        
            if (!Model::isValidId($id)) {
                throw new Exception('ID for getEducationById must be positive numeric');
            }

            $select = $app_db_connection->prepare('SELECT * FROM user_education WHERE id = :id LIMIT 1;');
            $select->bindParam(':id', $id);
            $select->execute();

            $row = $select->fetch();
            
            return $row !== FALSE
                ? new UserEducation($row)
                : NULL;
        }


        /** */
        #################################################
        # Getters
        #################################################
        /** */

        public function get($column_name) {
            return $this->$column_name;
        }


        /** */
        #################################################
        # Setters
        #################################################
        /** */

        public function setId($id) {

            if ($id === NULL) {
                $this->id = NULL;
                return;
            }

            if (!Model::isValidId($id)) {
                throw new Exception('ID for setId must be positive numeric');
            }

            $this->id = $id;
        }

        public function setLevel($level) {

            if ($level === NULL) return;

            if (!Model::isValidString($level)) {
                throw new Exception('Invalid level entered!');
            }

            $this->level = $level;
        }

        public function setTitle($title) {

            if ($title === NULL) return;

            if (!Model::isValidString($title)) {
                throw new Exception('Invalid company name entered!');
            }

            $this->title = $title;
        }

        public function setAwardingBody($awarding_body) {

            if ($awarding_body === NULL) return;

            if (!Model::isValidString($awarding_body)) {
                throw new Exception('Invalid awarding body entered!');
            }

            $this->awarding_body = $awarding_body;
        }

        public function setYearReceived($year_received) {

            if ($year_received === NULL) return;

            if (!Model::isValidString($year_received)) {
                throw new Exception('Invalid year received entered!');
            }

            $this->year_received = $year_received;
        }

        public function setUserId($user_id) {

            if ($user_id === NULL) {
                return;
            }

            if (!Model::isValidId($user_id)) {
                throw new Exception('ID for setUserId must be positive numeric');
            }

            $this->user_id = $user_id;
        }

    }

?>