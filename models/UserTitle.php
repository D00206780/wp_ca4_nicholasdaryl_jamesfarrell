<?php require_once('models/Model.php'); ?>

<?php 

    class UserTitle {

        private $id;
        private $name;

        public function __construct($properties) {

            if (!is_array($properties)) {
                throw new Exception('Invalid properties array passed to UserTitle constructor');
            }

            $this->setId($properties['id']      ?? NULL);
            $this->setName($properties['name']  ?? NULL);
        }


        /** */
        #################################################
        # Static Functions
        #################################################
        /** */

        public static function getAllTitles(PDO $app_db_connection) {

            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for UserTitle getAllTitles');
            }
            
            $select = $app_db_connection->prepare('SELECT * FROM user_titles;');
            $select->execute();

            return array_map(function($row) {
                return new UserTitle($row);
            }, $select->fetchAll());
        }

        public static function getTitleById(PDO $app_db_connection, $id) {
            
            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for UserTitle getTitleById');
            }
        
            if (!$id) return; 

            $id = (int) $id;
        
            if (!Model::isValidId($id)) {
                throw new Exception('ID for getTitleById must be positive numeric');
            }

            $select = $app_db_connection->prepare('SELECT * FROM user_titles WHERE id = :id LIMIT 1;');
            $select->bindParam(':id', $id);
            $select->execute();

            $row = $select->fetch();
            
            return $row !== FALSE
                ? new UserTitle($row)
                : NULL;
        }


        /** */
        #################################################
        # Validation Functions
        #################################################
        /** */

        public static function checkTitleIdValid(PDO $app_db_connection, $title_id) {

            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for UserTitle checkTitleIdValid');
            }

            return Model::checkIfExists($app_db_connection, 'user_titles', 'title_id', $title_id);
        }


        /** */
        #################################################
        # Getters
        #################################################
        /** */

        public function get($column_name) {
            return $this->$column_name;
        }


        /** */
        #################################################
        # Setters
        #################################################
        /** */

        public function setId($id) {

            if ($id === NULL) {
                $this->id = NULL;
                return;
            }

            if (!Model::isValidId($id)) {
                throw new Exception('ID for setId must be positive numeric');
            }

            $this->id = $id;
        }
        
        public function setName($name) {

            if ($name === NULL) return;

            if (!Model::isValidName($name)) {
                throw new Exception('Title must be between 2 and 55 characters long');
            }

            $this->name = $name;
        }
    }
?>