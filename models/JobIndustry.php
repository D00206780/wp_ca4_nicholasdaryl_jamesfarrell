<?php 

    class JobIndustry {

        private $id;
        private $name;

        public function __construct($properties) {

            if (!is_array($properties)) {
                throw new Exception('Invalid properties array passed to JobIndustry constructor');
            }

            $this->setId($properties['id']      ?? NULL);
            $this->setName($properties['name']  ?? NULL);
        }


        /** */
        #################################################
        # Static Functions
        #################################################
        /** */

        public static function getAllIndustries(PDO $app_db_connection) {

            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for JobIndustry getAllIndustries');
            }
            
            $select = $app_db_connection->prepare('SELECT * FROM job_industries;');
            $select->execute();

            return array_map(function($row) {
                return new JobIndustry($row);
            }, $select->fetchAll());
        }

        public static function getIndustryById(PDO $app_db_connection, $id) {
            
            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for JobIndustry getIndustryById');
            }
        
            $id = (int) $id;
        
            if (!Model::isValidId($id)) {
                throw new Exception('ID for getIndustryById must be positive numeric');
            }

            $select = $app_db_connection->prepare('SELECT * FROM job_industries WHERE id = :id LIMIT 1;');
            $select->bindParam(':id', $id);
            $select->execute();

            $row = $select->fetch();
            
            return $row !== FALSE
                ? new JobIndustry($row)
                : NULL;
        }


        /** */
        #################################################
        # Validation Functions
        #################################################
        /** */

        public static function checkIndustryIdValid(PDO $app_db_connection, $industry_id) {

            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for JobIndustry checkIndustryIdValid');
            }

            return Model::checkIfExists($app_db_connection, 'job_industries', 'industry_id', $industry_id);
        }

        /** */
        #################################################
        # Getters
        #################################################
        /** */

        public function get($column_name) {
            return $this->$column_name;
        }


        /** */
        #################################################
        # Setters
        #################################################
        /** */

        public function setId($id) {

            if ($id === NULL) {
                $this->id = NULL;
                return;
            }

            if (!Model::isValidId($id)) {
                throw new Exception('ID for setId must be positive numeric');
            }

            $this->id = $id;
        }
        
        public function setName($name) {

            if ($name === NULL) return;

            if (!Model::isValidString($name)) {
                throw new Exception('Industry must be between 2 and 55 characters long');
            }

            $this->name = $name;
        }

    }

?>