<?php require_once('models/Model.php'); ?>

<?php 

    class User {

        private $id;
        private $title_id;
        private $first_name;
        private $last_name;
        private $gender_id;
        private $contact_number;
        private $email_address;
        private $hash;
        private $address_line_1;
        private $address_line_2;
        private $town_city;
        private $county;
        private $country;
        private $account_type;
        private $cv_id;
    
        public function __construct($properties) {

            if (!is_array($properties)) {
                throw new Exception('Invalid properties array passed to User constructor');
            }

            $this->setId($properties['id']                          ?? NULL); 
            $this->setTitleId($properties['title_id']               ?? NULL);
            $this->setFirstName($properties['first_name']           ?? NULL);
            $this->setLastName($properties['last_name']             ?? NULL);
            $this->setGenderId($properties['gender_id']             ?? NULL);
            $this->setContactNumber($properties['contact_number']   ?? NULL);
            $this->setEmailAddress($properties['email_address']     ?? NULL);
            $this->setHash($properties['hash']                      ?? NULL);
            $this->generateHash($properties['password']             ?? NULL);
            $this->setAddressLine1($properties['address_line_1']    ?? NULL);
            $this->setAddressLine2($properties['address_line_2' ]   ?? NULL);
            $this->setTownCity($properties['town_city']             ?? NULL);
            $this->setCounty($properties['county']                  ?? NULL);
            $this->setCountry($properties['country']                ?? NULL);
            $this->setAccountType($properties['account_type']       ?? NULL);
            $this->setCVId($properties['cv_id']                     ?? NULL);
        
        }


        /** */
        #################################################
        # Instance Methods
        #################################################
        /** */

        public function save(PDO $app_db_connection) {

            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for User save');
            }

            if(!$this->get('id')) {
        
                $insert = $app_db_connection->prepare('INSERT INTO users (title_id, first_name, last_name, gender_id, contact_number, email_address, hash, address_line_1, address_line_2, town_city, county, country, account_type, cv_id) VALUES (:title_id, :first_name, :last_name, :gender_id, :contact_number, :email_address, :hash, :address_line_1, :address_line_2, :town_city, :county, :country, :account_type, :cv_id);');
                $insert->execute([
                    'title_id'       => $this->get('title_id')          ?? NULL,
                    'first_name'     => $this->get('first_name')        ?? NULL,
                    'last_name'      => $this->get('last_name')         ?? NULL,
                    'gender_id'      => $this->get('gender_id')         ?? NULL,
                    'contact_number' => $this->get('contact_number')    ?? NULL,
                    'email_address'  => $this->get('email_address')     ?? NULL,
                    'hash'           => $this->get('hash')              ?? NULL,
                    'address_line_1' => $this->get('address_line_1')    ?? NULL,
                    'address_line_2' => $this->get('address_line_2')    ?? NULL,
                    'town_city'      => $this->get('town_city')         ?? NULL,
                    'county'         => $this->get('county')            ?? NULL,
                    'country'        => $this->get('country')           ?? NULL,
                    'account_type'   => $this->get('account_type')      ?? NULL,
                    'cv_id'          => $this->get('cv_id')             ?? NULL,
                ]);

                $saved = $insert->rowCount() === 1;
            
                if ($saved) {
                    $this->setId($app_db_connection->lastInsertId());
                }

                return $saved;

            } else {

                $update = $app_db_connection->prepare('UPDATE users SET title_id = :title_id, first_name = :first_name, last_name = :last_name, gender_id = :gender_id, contact_number = :contact_number, email_address = :email_address, hash = :hash, address_line_1 = :address_line_1, address_line_2 = :address_line_2, town_city = :town_city, county = :county, country = :country, account_type = :account_type, cv_id = :cv_id WHERE id = :id;');
                $update->execute([
                    'id'             => $this->get('id')                ?? NULL, 
                    'title_id'       => $this->get('title_id')          ?? NULL,
                    'first_name'     => $this->get('first_name')        ?? NULL,
                    'last_name'      => $this->get('last_name')         ?? NULL,
                    'gender_id'      => $this->get('gender_id')         ?? NULL,
                    'contact_number' => $this->get('contact_number')    ?? NULL,
                    'email_address'  => $this->get('email_address')     ?? NULL,
                    'hash'           => $this->get('hash')              ?? NULL,
                    'address_line_1' => $this->get('address_line_1')    ?? NULL,
                    'address_line_2' => $this->get('address_line_2')    ?? NULL,
                    'town_city'      => $this->get('town_city')         ?? NULL,
                    'county'         => $this->get('county')            ?? NULL,
                    'country'        => $this->get('country')           ?? NULL,
                    'account_type'   => $this->get('account_type')      ?? NULL,
                    'cv_id'          => $this->get('cv_id')             ?? NULL,
                ]);

                return $update->rowCount() === 1;
            }

        }

        public function delete(PDO $app_db_connection) {

            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for User delete');
            }

            if ($this->get('id') === NULL) {
                throw new Exception('Cannot delete a transient User object');
            }
        
            $delete = $app_db_connection->prepare('DELETE FROM users WHERE id = :id;');
            $delete->bindParam(':id', $id);
            $delete->execute();
        
            $deleted = $delete->rowCount() === 1;
        
            if ($deleted) {
              $this->setId(NULL);
            }
        
            return $deleted;
        }


        /** */
        #################################################
        # Static Functions
        #################################################
        /** */

        public static function getUserById(PDO $app_db_connection, $id) {
            
            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for User getUserById');
            }
        
            $id = (int) $id;
        
            if (!Model::isValidId($id)) {
                throw new Exception('ID for getUserById must be positive numeric');
            }

            $select = $app_db_connection->prepare('SELECT * FROM users WHERE id = :id LIMIT 1;');
            $select->bindParam(':id', $id);
            $select->execute();

            $row = $select->fetch();
            
            return $row !== FALSE
                ? new User($row)
                : NULL;
        }

        public static function getUserByEmail(PDO $app_db_connection, $email_address) {
            
            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for User getUserByEmail');
            }

            if (!Model::isValidEmailAddress($email_address)) {
                throw new Exception('Email for getUserByEmail is invalid');
            }

            $select = $app_db_connection->prepare('SELECT * FROM users WHERE email_address = :email_address LIMIT 1;');
            $select->bindParam(':email_address', $email_address);
            $select->execute();

            $row = $select->fetch();
            
            return $row !== FALSE
                 ? new User($row)
                 : NULL;
        }

        public static function getAllUsers(PDO $app_db_connection) {

            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for User getAllUsers');
            }

            $select = $app_db_connection->prepare('SELECT * FROM users;');
            $select->execute();

            return array_map(function($row) {
                return new User($row);
            }, $select->fetchAll());
        }


        /** */
        #################################################
        # Getters
        #################################################
        /** */

        public function get($column_name) {
            return $this->$column_name;
        }


        /** */
        #################################################
        # Setters
        #################################################
        /** */

        public function setId($id) {

            if ($id === NULL) {
                $this->id = NULL;
                return;
            }

            if (!Model::isValidId($id)) {
                throw new Exception('ID for setId must be positive numeric');
            }

            $this->id = $id;
        }
        
        public function setTitleId($title_id) {

            if (!$title_id) return;

            if (!Model::isValidId($title_id)) {
                throw new Exception('ID for setTitleId must be positive numeric');
            }

            $this->title_id = $title_id;
        }

        public function setFirstName($first_name) {

            if (!$first_name) return;

            if (!Model::isValidName($first_name)) {
                throw new Exception('Forename must be between 1 and 55 characters long');
            }

            $this->first_name = $first_name;
        }

        public function setLastName($last_name) {

            if (!$last_name) return;

            if (!Model::isValidName($last_name)) {
                throw new Exception('Surname must be between 1 and 55 characters long');
            }

            $this->last_name = $last_name;
        }

        public function setGenderId($gender_id) {

            if (!$gender_id) return;
            
            if (!Model::isValidId($gender_id)) {
                throw new Exception('ID for setGenderId must be positive numeric');
            }

            $this->gender_id = $gender_id;
        }

        public function setContactNumber($contact_number) {

            if (!$contact_number) return;

            if (!Model::isValidContactNumber($contact_number)) {
                throw new Exception('Please enter a valid contact number');
            }

            $this->contact_number = $contact_number;
        }

        public function setEmailAddress($email_address) {

            if (!$email_address) return;

            if (!Model::isValidEmailAddress($email_address)) {
                throw new Exception('Please enter a valid email address!');
            }

            $this->email_address = $email_address;
        }

        public function setHash($hash) {
            
            if (!$hash) return;

            $this->hash = $hash;
        }

        public function generateHash($password) {

            if (!$password) return;

            if (!Model::isValidPassword($password)) {
                throw new Exception('Please enter a valid password!');
            }

            $hash = password_hash($password, PASSWORD_BCRYPT, ['cost' => 12]);

            $this->setHash($hash);

        }

        public function setAddressLine1($address_line_1) {

            if (!$address_line_1) return;

            if (!Model::isValidString($address_line_1)) {
                throw new Exception('Invalid address line 1');
            }

            $this->address_line_1 = $address_line_1;
        }
        
        public function setAddressLine2($address_line_2) {

            if (!$address_line_2) return;

            if (!Model::isValidString($address_line_2)) {
                throw new Exception('Invalid address line 2');
            }

            $this->address_line_2 = $address_line_2;
        }

        public function setTownCity($town_city) {

            if (!$town_city) return;

            if (!Model::isValidString($town_city)) {
                throw new Exception('Invalid town or city');
            }

            $this->town_city = $town_city;
        }

        public function setCounty($county) {

            if (!$county) return;

            if (!Model::isValidString($county)) {
                throw new Exception('Invalid county');
            }

            $this->county = $county;
        }

        public function setCountry($country) {

            if (!$country) return;

            if (!Model::isValidString($country)) {
                throw new Exception('Invalid country');
            }

            $this->country = $country;
        }

        public function setAccountType($account_type) {

            if (!$account_type) return;

            if (!($account_type == 0 || $account_type == 1)) {
                throw new Exception('Invalid account type selected');
            }

            $this->account_type = $account_type;
        }

        public function setCVId($cv_id) {

            if (!$cv_id) return;

            if (!Model::isValidId($cv_id)) {
                throw new Exception('ID for setCVId must be positive numeric');
            }

            $this->cv_id = $cv_id;
        }

    }

?>