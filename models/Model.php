<?php require_once('lib/utils/Validator.php') ?>

<?php 

    class Model {

        public static function isValidId($id) {
            if (!Validator::validValue($id)) return false;
            $id = Validator::sanitize($id, FILTER_SANITIZE_NUMBER_INT);
            if (!Validator::validate($id, FILTER_VALIDATE_INT)) return false;

            return ($id >= 1);
        }

        public static function isValidString($string) {
            if (!Validator::validValue($string)) return false;
            $string = Validator::sanitize($string, FILTER_SANITIZE_STRING);

            return true;
        }

        public static function isValidName($name) {
            if (!Validator::validValue($name)) return false;
            $name = Validator::sanitize($name, FILTER_SANITIZE_STRING);

            # Snippet taken from Shane Gavin
            return preg_match('/^[a-z]{1,55}$/i', $name);
        }

        public static function isValidContactNumber($contact_number) {
            if (!Validator::validValue($contact_number)) return false;
            $contact_number = Validator::sanitize($contact_number, FILTER_SANITIZE_STRING);
            
            # Snippet taken from previous project
            return preg_match('/^08[0-9]{1}-{1}[0-9]{3}-[0-9]{4}$/', $contact_number);
        }

        public static function isValidEmailAddress($email_address) {
            if (!Validator::validValue($email_address)) return false;
            $email_address = Validator::sanitize($email_address, FILTER_SANITIZE_EMAIL);
            
            return Validator::validate($email_address, FILTER_VALIDATE_EMAIL);
        }

        public static function isValidPassword($password) {
            if (!Validator::validValue($password)) return false;
            $password = Validator::sanitize($password, FILTER_SANITIZE_STRING);
            
            # Snippet taken from previous project
            return preg_match('/^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z!@#$%]{8,32}$/', $password);
        }

        public static function isValidAddress($address) {
            if (!Validator::validValue($address)) return false;
            $address = Validator::sanitize($address, FILTER_SANITIZE_STRING);

            return preg_match('/^[a-z]{1,55}$/i', $address);
        }

        # Check if a given value exists in a given column from a given table
        public static function checkIfExists(PDO $app_db_connection, $table_name, $column_name, $column_value) {

            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for ' . $table_name . ' checkIfExists');
            }

            $select = $app_db_connection->prepare("SELECT * FROM $table_name WHERE $column_name = :column_value LIMIT 1;");
            $select->bindParam(':column_value', $column_value);
            $select->execute();

            return $select->rowCount() === 1;
        }

    } 

?>