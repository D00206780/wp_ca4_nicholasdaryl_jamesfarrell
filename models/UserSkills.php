<?php require_once('models/Model.php'); ?>

<?php 

    class UserSkills {

        private $id;
        private $name;
        private $user_id;

        public function __construct($properties) {
    
            if (!is_array($properties)) {
                throw new Exception('Invalid properties array passed to UserSkills constructor');
            }

            $this->setId($properties['id']                          ?? NULL);
            $this->setName($properties['name']                      ?? NULL);
            $this->setUserId($properties['user_id']                 ?? NULL);
        
        }


        /** */
        #################################################
        # Instance Methods
        #################################################
        /** */

        public function save(PDO $app_db_connection) {

            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for UserSkills save');
            }

            if(!$this->get('id')) {
        
                $insert = $app_db_connection->prepare('INSERT INTO user_skills (name, user_id) VALUES (:name, :user_id);');
                $insert->execute([
                    'name'           => $this->get('name')           ?? NULL,
                    'user_id'        => $this->get('user_id')        ?? NULL,
                ]);

                $saved = $insert->rowCount() === 1;
            
                if ($saved) {
                    $this->setId($app_db_connection->lastInsertId());
                }

                return $saved;

            } else {

                $update = $app_db_connection->prepare('UPDATE user_skills SET name = :name WHERE id = :id;');
                $update->execute([
                    'id'             => $this->get('id')             ?? NULL,
                    'name'           => $this->get('name')           ?? NULL,
                ]);

                return $update->rowCount() === 1;
            }

        }

        public function delete(PDO $app_db_connection) {

            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for UserSkills delete');
            }

            if ($this->get('id') === NULL) {
                throw new Exception('Cannot delete a transient UserSkills object');
            }

            $delete = $app_db_connection->prepare('DELETE FROM user_skills WHERE id = :id;');
            $delete->bindParam(':id', $this->get('id'));
            $delete->execute();
        
            $deleted = $delete->rowCount() === 1;
        
            if ($deleted) {
              $this->setId(NULL);
            }
        
            return $deleted;
        }

        
        /** */
        #################################################
        # Static Functions
        #################################################
        /** */

        public static function getAllSkillsByUserId(PDO $app_db_connection, $user_id) {

            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for UserSkills getAllSkillsByUserId');
            }
        
            $user_id = (int) $user_id;
        
            if (!Model::isValidId($user_id)) {
                throw new Exception('ID for getAllSkillsByUserId must be positive numeric');
            }

            $select = $app_db_connection->prepare('SELECT * FROM user_skills WHERE user_id = :user_id;');
            $select->bindParam(':user_id', $user_id);
            $select->execute();

            return array_map(function($row) {
                return new UserSkills($row);
            }, $select->fetchAll());

        }

        public static function getSkillById(PDO $app_db_connection, $id) {
            
            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for UserSkills getGenderById');
            }
            
            if (!$id) return; 
        
            $id = (int) $id;
        
            if (!Model::isValidId($id)) {
                throw new Exception('ID for getSkillById must be positive numeric');
            }

            $select = $app_db_connection->prepare('SELECT * FROM user_skills WHERE id = :id LIMIT 1;');
            $select->bindParam(':id', $id);
            $select->execute();

            $row = $select->fetch();
            
            return $row !== FALSE
                ? new UserSkills($row)
                : NULL;
        }


        /** */
        #################################################
        # Getters
        #################################################
        /** */

        public function get($column_name) {
            return $this->$column_name;
        }


        /** */
        #################################################
        # Setters
        #################################################
        /** */

        public function setId($id) {

            if ($id === NULL) {
                $this->id = NULL;
                return;
            }

            if (!Model::isValidId($id)) {
                throw new Exception('ID for setId must be positive numeric');
            }

            $this->id = $id;
        }

        public function setName($name) {

            if ($name === NULL) return;

            if (!Model::isValidString($name)) {
                throw new Exception('Invalid skill entered!');
            }

            $this->name = $name;
        }

        public function setUserId($user_id) {

            if ($user_id === NULL) {
                return;
            }

            if (!Model::isValidId($user_id)) {
                throw new Exception('ID for setUserId must be positive numeric');
            }

            $this->user_id = $user_id;
        }

    }

?>