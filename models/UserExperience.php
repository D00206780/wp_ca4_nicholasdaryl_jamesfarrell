<?php require_once('models/Model.php'); ?>
<?php 

    class UserExperience {

        private $id;
        private $position;
        private $company_name;
        private $term;
        private $description;
        private $user_id;

        public function __construct($properties) {
    
            if (!is_array($properties)) {
                throw new Exception('Invalid properties array passed to UserExperience constructor');
            }

            $this->setId($properties['id']                    ?? NULL);
            $this->setPosition($properties['position']        ?? NULL);
            $this->setCompanyName($properties['company_name'] ?? NULL);
            $this->setTerm($properties['term']                ?? NULL);
            $this->setDescription($properties['description']  ?? NULL);
            $this->setUserId($properties['user_id']           ?? NULL);
        }


        /** */
        #################################################
        # Instance Methods
        #################################################
        /** */

        public function save(PDO $app_db_connection) {

            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for UserExperience save');
            }

            if(!$this->get('id')) {
        
                $insert = $app_db_connection->prepare('INSERT INTO user_experience (position, company_name, term, description, user_id) VALUES (:position, :company_name, :term, :description, :user_id);');
                $insert->execute([
                    'position'     => $this->get('position')        ?? NULL,
                    'company_name' => $this->get('company_name')    ?? NULL,
                    'term'         => $this->get('term')            ?? NULL,
                    'description'  => $this->get('description')     ?? NULL,
                    'user_id'      => $this->get('user_id')         ?? NULL,
                ]);

                $saved = $insert->rowCount() === 1;
            
                if ($saved) {
                    $this->setId($app_db_connection->lastInsertId());
                }

                return $saved;

            } else {

                $update = $app_db_connection->prepare('UPDATE user_experience SET position = :position, company_name = :company_name, term = :term, description = :description WHERE id = :id;');
                $update->execute([
                    'id'           => $this->get('id')              ?? NULL,
                    'position'     => $this->get('position')        ?? NULL,
                    'company_name' => $this->get('company_name')    ?? NULL,
                    'term'         => $this->get('term')            ?? NULL,
                    'description'  => $this->get('description')     ?? NULL,
                ]);

                return $update->rowCount() === 1;
            }

        }

        public function delete(PDO $app_db_connection) {

            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for UserExperience delete');
            }

            if ($this->get('id') === NULL) {
                throw new Exception('Cannot delete a transient UserExperience object');
            }
        
            $delete = $app_db_connection->prepare('DELETE FROM user_experience WHERE id = :id;');
            $delete->bindParam(':id', $this->get('id'));
            $delete->execute();
        
            $deleted = $delete->rowCount() === 1;
        
            if ($deleted) {
              $this->setId(NULL);
            }
        
            return $deleted;
        }

        
        /** */
        #################################################
        # Static Functions
        #################################################
        /** */

        public static function getAllExperienceByUserId(PDO $app_db_connection, $user_id) {

            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for UserExperience getAllExperienceByUserId');
            }
        
            $user_id = (int) $user_id;
        
            if (!Model::isValidId($user_id)) {
                throw new Exception('ID for getAllExperienceByUserId must be positive numeric');
            }

            $select = $app_db_connection->prepare('SELECT * FROM user_experience WHERE user_id = :user_id;');
            $select->bindParam(':user_id', $user_id);
            $select->execute();

            return array_map(function($row) {
                return new UserExperience($row);
            }, $select->fetchAll());

        }

        public static function getExperienceById(PDO $app_db_connection, $id) {
            
            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for UserExperience getExperienceById');
            }
            
            if (!$id) return; 
        
            $id = (int) $id;
        
            if (!Model::isValidId($id)) {
                throw new Exception('ID for getExperienceById must be positive numeric');
            }

            $select = $app_db_connection->prepare('SELECT * FROM user_experience WHERE id = :id LIMIT 1;');
            $select->bindParam(':id', $id);
            $select->execute();

            $row = $select->fetch();
            
            return $row !== FALSE
                ? new UserExperience($row)
                : NULL;
        }


        /** */
        #################################################
        # Getters
        #################################################
        /** */

        public function get($column_name) {
            return $this->$column_name;
        }


        /** */
        #################################################
        # Setters
        #################################################
        /** */

        public function setId($id) {

            if ($id === NULL) {
                $this->id = NULL;
                return;
            }

            if (!Model::isValidId($id)) {
                throw new Exception('ID for setId must be positive numeric');
            }

            $this->id = $id;
        }

        public function setPosition($position) {

            if ($position === NULL) return;

            if (!Model::isValidString($position)) {
                throw new Exception('Invalid position entered!');
            }

            $this->position = $position;
        }

        public function setCompanyName($company_name) {

            if ($company_name === NULL) return;

            if (!Model::isValidString($company_name)) {
                throw new Exception('Invalid company name entered!');
            }

            $this->company_name = $company_name;
        }

        public function setTerm($term) {

            if ($term === NULL) return;

            if (!Model::isValidString($term)) {
                throw new Exception('Invalid term entered!');
            }

            $this->term = $term;
        }

        public function setDescription($description) {

            if ($description === NULL) return;

            if (!Model::isValidString($description)) {
                throw new Exception('Invalid description entered!');
            }

            $this->description = $description;
        }

        public function setUserId($user_id) {

            if ($user_id === NULL) {
                return;
            }

            if (!Model::isValidId($user_id)) {
                throw new Exception('ID for setUserId must be positive numeric');
            }

            $this->user_id = $user_id;
        }

    }

?>