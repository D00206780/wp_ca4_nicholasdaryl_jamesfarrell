<?php require_once('models/Model.php'); ?>

<?php 

    class UserReferees {

        private $id;
        private $name;
        private $email_address;
        private $contact_number;
        private $description;
        private $user_id;

        public function __construct($properties) {
    
            if (!is_array($properties)) {
                throw new Exception('Invalid properties array passed to UserReferees constructor');
            }

            $this->setId($properties['id']                          ?? NULL);
            $this->setName($properties['name']                      ?? NULL);
            $this->setContactNumber($properties['contact_number']   ?? NULL);
            $this->setEmailAddress($properties['email_address']     ?? NULL);
            $this->setDescription($properties['description']        ?? NULL);
            $this->setUserId($properties['user_id']                 ?? NULL);
        }


        /** */
        #################################################
        # Instance Methods
        #################################################
        /** */

        public function save(PDO $app_db_connection) {

            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for UserReferees save');
            }

            if(!$this->get('id')) {
        
                $insert = $app_db_connection->prepare('INSERT INTO user_referees (name, contact_number, email_address, description, user_id) VALUES (:name, :contact_number, :email_address, :description, :user_id);');
                $insert->execute([
                    'name'           => $this->get('name')           ?? NULL,
                    'contact_number' => $this->get('contact_number') ?? NULL,
                    'email_address'  => $this->get('email_address')  ?? NULL,
                    'description'    => $this->get('description')    ?? NULL,
                    'user_id'        => $this->get('user_id')        ?? NULL,
                ]);

                $saved = $insert->rowCount() === 1;
            
                if ($saved) {
                    $this->setId($app_db_connection->lastInsertId());
                }

                return $saved;

            } else {

                $update = $app_db_connection->prepare('UPDATE user_referees SET name = :name, contact_number = :contact_number, email_address = :email_address, description = :description WHERE id = :id;');
                $update->execute([
                    'id'             => $this->get('id')             ?? NULL,
                    'name'           => $this->get('name')           ?? NULL,
                    'contact_number' => $this->get('contact_number') ?? NULL,
                    'email_address'  => $this->get('email_address')  ?? NULL,
                    'description'    => $this->get('description')    ?? NULL,
                ]);

                return $update->rowCount() === 1;
            }

        }

        public function delete(PDO $app_db_connection) {

            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for UserReferees delete');
            }

            if ($this->get('id') === NULL) {
                throw new Exception('Cannot delete a transient UserReferees object');
            }
        
            $delete = $app_db_connection->prepare('DELETE FROM user_referees WHERE id = :id;');
            $delete->bindParam(':id', $this->get('id'));
            $delete->execute();
        
            $deleted = $delete->rowCount() === 1;
        
            if ($deleted) {
              $this->setId(NULL);
            }
        
            return $deleted;
        }

        
        /** */
        #################################################
        # Static Functions
        #################################################
        /** */

        public static function getAllRefereesByUserId(PDO $app_db_connection, $user_id) {

            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for UserReferees getAllRefereesByUserId');
            }
        
            $user_id = (int) $user_id;
        
            if (!Model::isValidId($user_id)) {
                throw new Exception('ID for getAllRefereesByUserId must be positive numeric');
            }

            $select = $app_db_connection->prepare('SELECT * FROM user_referees WHERE user_id = :user_id;');
            $select->bindParam(':user_id', $user_id);
            $select->execute();

            return array_map(function($row) {
                return new UserReferees($row);
            }, $select->fetchAll());

        }

        public static function getRefereeById(PDO $app_db_connection, $id) {
            
            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for UserReferees getRefereeById');
            }
            
            if (!$id) return; 
        
            $id = (int) $id;
        
            if (!Model::isValidId($id)) {
                throw new Exception('ID for getRefereeById must be positive numeric');
            }

            $select = $app_db_connection->prepare('SELECT * FROM user_referees WHERE id = :id LIMIT 1;');
            $select->bindParam(':id', $id);
            $select->execute();

            $row = $select->fetch();
            
            return $row !== FALSE
                ? new UserReferees($row)
                : NULL;
        }

        /** */
        #################################################
        # Getters
        #################################################
        /** */

        public function get($column_name) {
            return $this->$column_name;
        }


        /** */
        #################################################
        # Setters
        #################################################
        /** */

        public function setId($id) {

            if ($id === NULL) {
                $this->id = NULL;
                return;
            }

            if (!Model::isValidId($id)) {
                throw new Exception('ID for setId must be positive numeric');
            }

            $this->id = $id;
        }

        public function setName($name) {

            if ($name === NULL) return;

            if (!Model::isValidString($name)) {
                throw new Exception('Invalid name entered!');
            }

            $this->name = $name;
        }

        public function setContactNumber($contact_number) {

            if (!$contact_number) return;

            if (!Model::isValidContactNumber($contact_number)) {
                throw new Exception('Please enter a valid contact number');
            }

            $this->contact_number = $contact_number;
        }

        public function setEmailAddress($email_address) {

            if (!$email_address) return;

            if (!Model::isValidEmailAddress($email_address)) {
                throw new Exception('Please enter a valid email address!');
            }

            $this->email_address = $email_address;
        }

        public function setDescription($description) {

            if ($description === NULL) return;

            if (!Model::isValidString($description)) {
                throw new Exception('Invalid description entered!');
            }

            $this->description = $description;
        }

        public function setUserId($user_id) {

            if ($user_id === NULL) {
                return;
            }

            if (!Model::isValidId($user_id)) {
                throw new Exception('ID for setUserId must be positive numeric');
            }

            $this->user_id = $user_id;
        }

    }

?>