<?php 

    class JobApplication {

        private $user_id;
        private $job_id;
        private $status;

        public function __construct($properties) {

            if (!is_array($properties)) {
                throw new Exception('Invalid properties array passed to JobApplication constructor');
            }

            $this->setUserId($properties['user_id'] ?? NULL);
            $this->setJobId($properties['job_id']   ?? NULL);
            $this->setStatus($properties['status']  ?? NULL);
        }


        /** */
        #################################################
        # Static Functions
        #################################################
        /** */

        public static function getAllJobApplicationsFromAUser(PDO $app_db_connection, $user_id) {

            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for Application getAllJobApplications');
            }
            
            $select = $app_db_connection->prepare('SELECT * FROM job_applications WHERE user_id = :user_id;');
            $select->bindParam(':user_id', $user_id);
            $select->execute();

            return array_map(function($row) {
                return new JobApplication($row);
            }, $select->fetchAll());
        }

        public static function getAllJobApplicationsByJobId(PDO $app_db_connection, $user_id) {

            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for Application getAllJobApplications');
            }
            
            $select = $app_db_connection->prepare('SELECT * FROM job_applications WHERE job_id = :user_id;');
            $select->bindParam(':user_id', $user_id);
            $select->execute();

            return array_map(function($row) {
                return new JobApplication($row);
            }, $select->fetchAll());
        }

        public static function getJobApplicationById(PDO $app_db_connection, $user_id, $job_id) {
            
            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for JobIndustry getIndustryById');
            }
        
            $user_id = (int) $user_id;
            $job_id = (int) $job_id;
        
            if (!Model::isValidId($user_id) && !Model::isValidId($job_id)) {
                throw new Exception('IDs for getJobApplicationById must be positive numerics');
            }

            $select = $app_db_connection->prepare('SELECT * FROM job_applications WHERE user_id = :user_id AND job_id = :job_id LIMIT 1;');
            $select->bindParam(':user_id', $user_id);
            $select->bindParam(':job_id', $job_id);
            $select->execute();

            $row = $select->fetch();
            
            return $row !== FALSE
                ? new JobApplication($row)
                : NULL;
        }

        public static function createJobApplication(PDO $app_db_connection, $user_id, $job_id) {

            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for Application getAllJobApplications');
            }
            
            $select = $app_db_connection->prepare('INSERT INTO job_applications (user_id, job_id) VALUES (:user_id, :job_id)');
            $select->bindParam(':user_id', $user_id);
            $select->bindParam(':job_id', $job_id);
            $select->execute();

        }

        public static function deleteJobApplication(PDO $app_db_connection, $user_id, $job_id) {

            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for Application getAllJobApplications');
            }
            
            $select = $app_db_connection->prepare('DELETE FROM job_applications WHERE user_id = :user_id AND job_id = :job_id');
            $select->bindParam(':user_id', $user_id);
            $select->bindParam(':job_id', $job_id);
            $select->execute();

        }
        public static function acceptJobApplication(PDO $app_db_connection, $user_id, $job_id) {

            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for Application getAllJobApplications');
            }
            
            $select = $app_db_connection->prepare('UPDATE job_applications SET status = 1 WHERE job_id = :job_id AND user_id = :user_id');
            $select->bindParam(':user_id', $user_id);
            $select->bindParam(':job_id', $job_id);
            $select->execute();

        }

        public static function rejectJobApplication(PDO $app_db_connection, $user_id, $job_id) {

            if (!($app_db_connection instanceof PDO)) {
                throw new Exception('Invalid PDO object for Application getAllJobApplications');
            }
            
            $select = $app_db_connection->prepare('UPDATE job_applications SET status = -1 WHERE job_id = :job_id AND user_id = :user_id');
            $select->bindParam(':user_id', $user_id);
            $select->bindParam(':job_id', $job_id);
            $select->execute();

        }
        /** */
        #################################################
        # Getters
        #################################################
        /** */

        public function get($column_name) {
            return $this->$column_name;
        }


        /** */
        #################################################
        # Setters
        #################################################
        /** */

        public function setUserId($id) {

            if ($id === NULL) {
                $this->id = NULL;
                return;
            }

            if (!Model::isValidId($id)) {
                throw new Exception('ID for setId must be positive numeric');
            }

            $this->user_id = $id;
        }
        
        public function setJobId($id) {

            if ($id === NULL) {
                $this->id = NULL;
                return;
            }

            if (!Model::isValidId($id)) {
                throw new Exception('ID for setId must be positive numeric');
            }

            $this->job_id = $id;
        }

        public function setStatus($status) {

            $this->status = $status;
        }

    }

?>