-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 02, 2019 at 09:44 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wp_ca4_nicholasdaryl_jamesfarrell`
--

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_swedish_ci NOT NULL,
  `company` varchar(255) COLLATE utf8_swedish_ci NOT NULL,
  `description` text COLLATE utf8_swedish_ci NOT NULL,
  `location` varchar(255) COLLATE utf8_swedish_ci NOT NULL,
  `term` varchar(255) COLLATE utf8_swedish_ci NOT NULL,
  `rate` varchar(255) COLLATE utf8_swedish_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `industry_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `title`, `company`, `description`, `location`, `term`, `rate`, `user_id`, `industry_id`, `category_id`) VALUES
(1, 'DevOps Intern', 'Amazon', 'Come work for Amazon!', 'Dublin', '6 month internship', '€550 per month', 37, 4, 5),
(2, 'DevOps Intern', 'Amazon', 'Come work for Amazon!', 'Dublin', '6 month internship', '€550 per month', 37, 4, 5),
(3, 'title', 'company', 'asdasdasdasdasdads', 'dundalk', '1', '1', 38, 20, 1),
(4, 'Chair Stacking', 'IKEA', 'Stack em up', 'dundalk', '1', '1', 37, 1, 2),
(5, 'Better Chair Stacking', 'IKEA', 'Stack em up even more', 'dundalk', '1', '£0 $0 100 pieces of leaves per second', 41, 5, 1),
(6, 'new', 'wewe', 'wewe', 'asdffad', 'adsfsadfasdf', 'asdfadsfa', 44, 10, 2);

-- --------------------------------------------------------

--
-- Table structure for table `job_applications`
--

CREATE TABLE `job_applications` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `job_id` bigint(20) UNSIGNED NOT NULL,
  `status` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Dumping data for table `job_applications`
--

INSERT INTO `job_applications` (`user_id`, `job_id`, `status`) VALUES
(32, 1, 0),
(42, 2, 0),
(42, 2, 0),
(42, 1, 0),
(42, 6, 0);

-- --------------------------------------------------------

--
-- Table structure for table `job_categories`
--

CREATE TABLE `job_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_swedish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Dumping data for table `job_categories`
--

INSERT INTO `job_categories` (`id`, `name`) VALUES
(1, 'Full-time'),
(2, 'Part-time'),
(3, 'Contract'),
(4, 'Temporary'),
(5, 'Internship'),
(6, 'Other                              ');

-- --------------------------------------------------------

--
-- Table structure for table `job_industries`
--

CREATE TABLE `job_industries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_swedish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Dumping data for table `job_industries`
--

INSERT INTO `job_industries` (`id`, `name`) VALUES
(1, 'Aerospace'),
(2, 'Agriculture'),
(3, 'Chemical'),
(4, 'Computer'),
(5, 'Construction'),
(6, 'Defense'),
(7, 'Education'),
(8, 'Energy'),
(9, 'Entertainment'),
(10, 'Financial Services'),
(11, 'Food'),
(12, 'Health Care'),
(13, 'Hospitality'),
(14, 'Information'),
(15, 'Manufacturing'),
(16, 'Mass Media'),
(17, 'Mining'),
(18, 'Telecommunications'),
(19, 'Transport'),
(20, 'Water'),
(21, 'Service');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title_id` bigint(20) UNSIGNED DEFAULT NULL,
  `first_name` varchar(55) COLLATE utf8_swedish_ci NOT NULL,
  `last_name` varchar(55) COLLATE utf8_swedish_ci NOT NULL,
  `gender_id` bigint(20) UNSIGNED DEFAULT NULL,
  `contact_number` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `email_address` varchar(255) COLLATE utf8_swedish_ci NOT NULL,
  `hash` varchar(255) COLLATE utf8_swedish_ci NOT NULL,
  `address_line_1` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `address_line_2` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `town_city` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `county` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `account_type` tinyint(1) DEFAULT NULL,
  `cv_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `title_id`, `first_name`, `last_name`, `gender_id`, `contact_number`, `email_address`, `hash`, `address_line_1`, `address_line_2`, `town_city`, `county`, `country`, `account_type`, `cv_id`) VALUES
(1, 2, 'James', 'Farrell', 1, NULL, 'jamesfarrell16@hotmail.com', '$2y$12$aRFPHlFvJwg8T2T3cFp0wOPMNMnELHfJ5mvOSf5Fc5G/lYxG0pIU.', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(32, NULL, 'James', 'Farrell', NULL, NULL, 'jamesfarrell17@hotmail.com', '$2y$12$s76566vfPYqo3A6GUjwRLeV.OikyFrODvqD6LU7WLFmw3f9QDpoMi', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(33, NULL, 'James', 'Farrell', NULL, NULL, 'jamesfarrell18@hotmail.com', '$2y$12$rvUgHe3FJ/yLjelRdBlJZOGr3CYfPCh4Lus.U9TtvkqNN3NGp.xsa', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(36, NULL, 'James', 'Farrell', NULL, NULL, 'jamesfarrell19@hotmail.com', '$2y$12$j.QLvoQ/KJfhunT5jScUP.ol9nYaHFzcR3dC.ORU4H8Y9gU8Lwshe', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(37, NULL, 'Nick', 'Daryl', NULL, NULL, 'nicholasDaryl@nicksEmail.com', '$2y$12$ALweSIQPPQP8UNJMssj66Ot9EtHX9LVW47CAY/qVZmqP3k0FxrPAq', NULL, NULL, NULL, NULL, NULL, 1, NULL),
(38, 1, 'John', 'user', NULL, NULL, 'test@gmail.com', '$2y$12$GXDllENYNfmYt9GD17Ag3OTAGmt4SMQbRiGysPuTS4DAi203bAm2y', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(39, NULL, 'test', 'user', NULL, NULL, 'test2@gmail.com', '$2y$12$Rrd8WOdjdarPjO9C7ddxYuWobjlZgMYW/avjr.P/nIgrVx6ExG50S', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(41, NULL, 'The', 'Employer', 3, NULL, 'theEmployer@gmail.com', 'Password', NULL, NULL, NULL, NULL, NULL, 1, NULL),
(42, NULL, 'New', 'New', 1, NULL, 'new@gmail.com', '$2y$12$axjaC0m46Rai2qcqSe6QEuHXaoho5zVNwwUEGOt2qG.vy/9WpZ6/W', 'road', 'planet earth', 'burger town', 'Milky Way', 'Russia??', NULL, NULL),
(43, NULL, 'Another', 'user', NULL, NULL, 'email@gmail.com', '$2y$12$dZvHdxyzNPsvxuK7RRZk5urc2Q17Xj5dLh2QaFzyxlJt3nVLnRjbC', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(44, NULL, 'employer', 'the', NULL, NULL, 'employer@gmail.com', '$2y$12$Xmm/CLpe1okJB.YWfJvCV.Te8KLn/1nGHsf0hYBEyVJUCY6KHp3d6', NULL, NULL, NULL, NULL, NULL, 1, NULL),
(45, NULL, 'Another', 'user', NULL, NULL, 'email1@gmail.com', '$2y$12$nlIYPXdKAregmK8mXGUp0O1js9lyiB0e2bc4UZh1jbmVkKQpwfo4i', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_cv`
--

CREATE TABLE `user_cv` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `introduction` text COLLATE utf8_swedish_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Dumping data for table `user_cv`
--

INSERT INTO `user_cv` (`id`, `introduction`, `user_id`) VALUES
(1, 'Hello', 0),
(2, 'Edited again', 32),
(3, 'Edited again', 32),
(4, 'Changed', 38),
(5, 'Introduction test\r\n', 42),
(6, 'changed', 43),
(7, 'Added new introduction text', 45);

-- --------------------------------------------------------

--
-- Table structure for table `user_education`
--

CREATE TABLE `user_education` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `level` varchar(255) COLLATE utf8_swedish_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_swedish_ci NOT NULL,
  `awarding_body` varchar(255) COLLATE utf8_swedish_ci NOT NULL,
  `year_received` varchar(255) COLLATE utf8_swedish_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Dumping data for table `user_education`
--

INSERT INTO `user_education` (`id`, `level`, `title`, `awarding_body`, `year_received`, `user_id`) VALUES
(4, '8', 'Bachelors', 'Dkit', '2021', 42),
(5, 'sdfsdf', 'sdfsdf', 'sdfsdf', 'sdfsdfsdf', 43),
(6, 'Changed', 'Changed', 'Changed', 'Changed', 45);

-- --------------------------------------------------------

--
-- Table structure for table `user_experience`
--

CREATE TABLE `user_experience` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `position` varchar(255) COLLATE utf8_swedish_ci NOT NULL,
  `company_name` varchar(255) COLLATE utf8_swedish_ci NOT NULL,
  `term` varchar(255) COLLATE utf8_swedish_ci NOT NULL,
  `description` text COLLATE utf8_swedish_ci,
  `user_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Dumping data for table `user_experience`
--

INSERT INTO `user_experience` (`id`, `position`, `company_name`, `term`, `description`, `user_id`) VALUES
(2, 'manager', 'Google', '1 year', 'changed', 42);

-- --------------------------------------------------------

--
-- Table structure for table `user_genders`
--

CREATE TABLE `user_genders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(10) COLLATE utf8_swedish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Dumping data for table `user_genders`
--

INSERT INTO `user_genders` (`id`, `name`) VALUES
(1, 'Male'),
(2, 'Female'),
(3, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `user_referees`
--

CREATE TABLE `user_referees` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_swedish_ci NOT NULL,
  `email_address` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `contact_number` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Dumping data for table `user_referees`
--

INSERT INTO `user_referees` (`id`, `name`, `email_address`, `contact_number`, `description`, `user_id`) VALUES
(1, 'saads', 'sdaffsda', 'asdfdsa', 'asffasdsfda', 32),
(3, 'Nicholas', 'nicholasdaryl@gmail.com', '083-929-2929', 'hello', 42);

-- --------------------------------------------------------

--
-- Table structure for table `user_skills`
--

CREATE TABLE `user_skills` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_swedish_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Dumping data for table `user_skills`
--

INSERT INTO `user_skills` (`id`, `name`, `user_id`) VALUES
(5, 'asd', 32),
(8, 'New', 32),
(9, 'Nice', 42),
(10, '2nd', 42),
(12, 'new', 43),
(14, 'Changed Skill', 45),
(15, 'Changed Skill', 45),
(16, 'Changed Skill', 45);

-- --------------------------------------------------------

--
-- Table structure for table `user_titles`
--

CREATE TABLE `user_titles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_swedish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Dumping data for table `user_titles`
--

INSERT INTO `user_titles` (`id`, `name`) VALUES
(1, 'Ms'),
(2, 'Mr'),
(3, 'Mrs'),
(4, 'BSc'),
(5, 'MSc'),
(6, 'PhD');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `industry_id` (`industry_id`),
  ADD KEY `type_id` (`category_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `job_applications`
--
ALTER TABLE `job_applications`
  ADD KEY `user_id` (`user_id`),
  ADD KEY `job_id` (`job_id`);

--
-- Indexes for table `job_categories`
--
ALTER TABLE `job_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_industries`
--
ALTER TABLE `job_industries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email_address` (`email_address`),
  ADD UNIQUE KEY `cv_id` (`cv_id`),
  ADD UNIQUE KEY `title_id` (`title_id`),
  ADD KEY `gender_id` (`gender_id`);

--
-- Indexes for table `user_cv`
--
ALTER TABLE `user_cv`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_education`
--
ALTER TABLE `user_education`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `user_experience`
--
ALTER TABLE `user_experience`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `user_genders`
--
ALTER TABLE `user_genders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_referees`
--
ALTER TABLE `user_referees`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `user_skills`
--
ALTER TABLE `user_skills`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `user_titles`
--
ALTER TABLE `user_titles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `job_categories`
--
ALTER TABLE `job_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `job_industries`
--
ALTER TABLE `job_industries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `user_cv`
--
ALTER TABLE `user_cv`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `user_education`
--
ALTER TABLE `user_education`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user_experience`
--
ALTER TABLE `user_experience`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_genders`
--
ALTER TABLE `user_genders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_referees`
--
ALTER TABLE `user_referees`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_skills`
--
ALTER TABLE `user_skills`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `user_titles`
--
ALTER TABLE `user_titles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `jobs`
--
ALTER TABLE `jobs`
  ADD CONSTRAINT `jobs_ibfk_1` FOREIGN KEY (`industry_id`) REFERENCES `job_industries` (`id`),
  ADD CONSTRAINT `jobs_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `job_categories` (`id`);

--
-- Constraints for table `job_applications`
--
ALTER TABLE `job_applications`
  ADD CONSTRAINT `job_applications_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `job_applications_ibfk_2` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`title_id`) REFERENCES `user_titles` (`id`),
  ADD CONSTRAINT `users_ibfk_2` FOREIGN KEY (`cv_id`) REFERENCES `user_cv` (`id`),
  ADD CONSTRAINT `users_ibfk_3` FOREIGN KEY (`gender_id`) REFERENCES `user_genders` (`id`);

--
-- Constraints for table `user_education`
--
ALTER TABLE `user_education`
  ADD CONSTRAINT `user_education_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `user_experience`
--
ALTER TABLE `user_experience`
  ADD CONSTRAINT `user_experience_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `user_referees`
--
ALTER TABLE `user_referees`
  ADD CONSTRAINT `user_referees_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `user_skills`
--
ALTER TABLE `user_skills`
  ADD CONSTRAINT `user_skills_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
