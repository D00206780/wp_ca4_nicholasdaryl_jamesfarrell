window.onload = function() {

    // Store form references
    const loginForm = document.querySelector('#modal_login_form');
    const registerForm = document.querySelector('#modal_register_form');
    const registerFirstName = document.querySelector('#register_first_name');
    const registerLastName = document.querySelector('#register_last_name');
    const registerEmailAddress = document.querySelector('#register_email_address');
    const registerPassword = document.querySelector('#register_password');
    const registerConfirmPassword = document.querySelector('#register_confirm_password');
    const registerAccountType = document.querySelector('#register_account_type');

    //If the login form element has been stored -
    if (loginForm) handleLogin(loginForm);

    //If the register form element has been stored -
    if (registerForm) handleRegister(registerForm);
    
    //If the register first name element has been stored -
    if (registerFirstName) checkFirstName(registerFirstName);

    //If the register last name element has been stored -
    if (registerLastName) checkLastName(registerLastName);

    //If the register form email element has been stored -
    if (registerEmailAddress) checkEmailAddress(registerEmailAddress);

    //If the register form password element has been stored -
    if (registerPassword) checkPassword(registerPassword);

    //If the register form confirm password element has been stored -
    if (registerConfirmPassword) checkConfirmPassword(registerConfirmPassword);

    //If the register form account status element has been stored -
    if (registerAccountType) checkAccountType(registerAccountType);

};

//Handle login data 
handleLogin = function(loginForm) {

    //Store references
    let loginFormErrors = document.querySelector('#login_form_errors');

    //Listen for submissions
    loginForm.addEventListener('submit', event => {

        //Prevent submission
        event.preventDefault();
        
        //Retrieve data
        const loginData = {
            emailAddress  : document.querySelector('#login_email_address').value,
            password      : document.querySelector('#login_password').value,
        };

        //Post data
        fetch('api/validateLogin', createFetchConfig(loginData))
            .then(res  => res.json())
                .then(data => {
                    
                    //Parse
                    data = JSON.parse(data)

                    //If errors -
                    if (data.errors) {

                        //- Display
                        loginFormErrors.innerHTML = data.errors;

                    } else {

                        //- Submit
                        loginForm.submit();

                    }

            });

    });

}

//Handle registration data
handleRegister = function(registerForm) {

    //Store references
    let registerFormErrors = document.querySelector('#modal_register_errors');
    let formInputFields = document.querySelectorAll('#modal_register_form input, #modal_register_form select');

    //Assume valid
    let formValid = true;

    //Add listener
    registerForm.addEventListener('submit', event => {

        //Prevent submission
        event.preventDefault();

        //Retrieve data
        const registerData = {
            firstName       : document.querySelector('#register_first_name').value,
            lastName        : document.querySelector('#register_last_name').value,
            emailAddress    : document.querySelector('#register_email_address').value,
            password        : document.querySelector('#register_password').value,
            confirmPassword : document.querySelector('#register_confirm_password').value,
            accountType     : document.querySelector('#register_account_type').value,
        };

        //This is a security issue as users have the ability to tamper with form
        //element classes. However, this is currently just a demonstration. Further 
        //validation will take place on the server. 
        
        //Iterate over input fields 
        formInputFields.forEach(function (inputField) {

            //If input field is invalid
            if (inputField.classList.contains('is-invalid')) {

                //Set form valid to false
                formValid = false;

            }

        });

        //Check form validity
        if (formValid) {

            //Post data
            fetch('api/validateRegistration', createFetchConfig(registerData))
                .then(res  => res.json())
                    .then(data => {

                        //Parse
                        data = JSON.parse(data)

                        //If errors -
                        if (data.errors) {

                            //- Display
                            registerFormErrors.innerHTML = data.errors;

                        } else {

                            //- Submit
                            registerForm.submit();

                        }

                    });


        } else {

            //Report 
            document.querySelector('#register_form_errors').innerHTML = 'Please make sure to fill in the form correctly!';

        }

    });

}

//Validate first name
checkFirstName = function(firstName) {

    //Store references
    let firstNameFeedback = document.querySelector('#register_first_name_feedback');
    let firstNameField = document.querySelector('#register_first_name');

    //Listen for input
    firstName.addEventListener('input', event => {

        //Store values
        let firstName = event.target.value;

        //Check for match
        if (!firstName.match(/^[a-z]{1,}$/i)) {

            //Report
            firstNameFeedback.innerHTML = 'Please enter a valid name!';
            firstNameField.classList = 'form-control is-invalid';

        } else {

            //Report
            firstNameField.classList = 'form-control is-valid';

        }

    });

}

//Validate last name
checkLastName = function(lastName) {

    //Store references
    let lastNameFeedback = document.querySelector('#register_last_name_feedback');
    let lastNameField = document.querySelector('#register_last_name');

    //Listen for input
    lastName.addEventListener('input', event => {

        //Store values
        let lastName = event.target.value;

        //Check for match
        if (!lastName.match(/^[a-z]{1,}$/i)) {

            //Report
            lastNameFeedback.innerHTML = 'Please enter a valid name!';
            lastNameField.classList = 'form-control is-invalid';

        } else {

            //Report
            lastNameField.classList = 'form-control is-valid';

        }

    });

}

//Validate email address
checkEmailAddress = function(registerFormEmailAddress) {

    //Store references
    let emailAddressFeedback = document.querySelector('#register_email_address_feedback');
    let emailAddressField    = document.querySelector('#register_email_address');

    //Listen for input
    registerFormEmailAddress.addEventListener('input', event => {

        //Store values
        let emailAddress = event.target.value;

        //Snippet taken from: https://stackoverflow.com/questions/46155/how-to-validate-an-email-address-in-javascript
        if (emailAddress.match(/^(([^<>()\[\]\\.,;:\s@']+(\.[^<>()\[\]\\.,;:\s@']+)*)|('.+'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {

            //Store data
            const emailAddressCheck = {
                table  : 'users',
                column : 'email_address',
                value  : emailAddress,
            }

            //Post data
            fetch('api/checkValueExists', createFetchConfig(emailAddressCheck))
                .then(res  => res.json())
                    .then(data => {
                        
                        //Parse
                        data = JSON.parse(data)

                        //Check for errors
                        if (data.errors) {

                            //Report
                            emailAddressFeedback.innerHTML = data.errors;
                            emailAddressField.classList = 'form-control is-invalid';

                        } else if (data.exists) {

                            //Report
                            emailAddressFeedback.innerHTML = 'A user with that email address already exists!';
                            emailAddressField.classList = 'form-control is-invalid';

                        } else {

                            //Report
                            emailAddressField.classList = 'form-control is-valid';

                        }

                    });

        } else {

            //Report
            emailAddressFeedback.innerHTML = 'Please enter a valid email address!';
            emailAddressField.classList = 'form-control is-invalid';

        }

    });

}

//Validate password
checkPassword = function(registerFormPassword) {

    //Store references
    let passwordFeedback = document.querySelector('#register_password_feedback');
    let passwordField = document.querySelector('#register_password');

    //Listen for input
    registerFormPassword.addEventListener('input', event => {

        //Store input
        let password = event.target.value;

        //Create tests
        let lowercase = /[a-z]/;
        let uppercase = /[A-Z]/;
        let number    = /\d/;

        //Assume invalid
        passwordField.classList = 'form-control is-invalid';

        //Check password
        if (password.length <= 0) {

            passwordFeedback.innerHTML = 'Please enter a password';

        } else if (!lowercase.test(password)) {

            passwordFeedback.innerHTML = 'Must contain one lowercase letter';

        } else if (!uppercase.test(password)) {
            
            passwordFeedback.innerHTML = 'Must contain one uppercase letter';

        } else if (!number.test(password)) {

            passwordFeedback.innerHTML = 'Must contain one number';

        } else if (password.length < 8) {

            passwordFeedback.innerHTML = 'Must be atleast 8 characters long';

        } else if (password.length > 32) {

            passwordFeedback.innerHTML = 'Must not exceed 32 characters';

        } else {

            passwordFeedback.innerHTML = '';
            passwordField.classList = 'form-control is-valid';

        }

        //Check match
        checkPasswordMatch();

        //Banana!
        if (event.target.value === 'Monkeys!') {
            event.target.value = '';   
            alert('Bananas!');
        }

    });

}

//Validate password match
checkPasswordMatch = function() {

    //Store references
    let confirmPasswordFeedback = document.querySelector('#register_confirm_password_feedback');
    let confirmPasswordField = document.querySelector('#register_confirm_password');
    let passwordField = document.querySelector('#register_password');

    //Store values
    let confirmPassword = confirmPasswordField.value;
    let password = passwordField.value;

    //Check for match
    if (confirmPassword !== password) {

        //Report
        confirmPasswordFeedback.innerHTML = 'Passwords do not match!';
        confirmPasswordField.classList = 'form-control is-invalid';

    } else if (password === '' || confirmPassword === ''){

        //Report
        confirmPasswordField.classList = 'form-control';

    } else {

        //Report
        confirmPasswordField.classList = 'form-control is-valid';

    }

}

//Validate confirm password
checkConfirmPassword = function(registerConfirmPassword) {

    //Listen for input
    registerConfirmPassword.addEventListener('input', event => {

        //Check match
        checkPasswordMatch();

    });

}

//Validate account type
checkAccountType = function(registerFormAccountType) {

    //Listen for changes
    registerFormAccountType.addEventListener('change', event => {

        //Store references
        let accountType = event.target.value;

        //Check if set
        if (accountType === '') {

            //Report
            document.querySelector('#register_account_type').classList = 'custom-select is-invalid';

        } else {

            //Report
            document.querySelector('#register_account_type').classList = 'custom-select is-valid';

        }

    });

}

//Configure a fetch post call
createFetchConfig = function(data) {

    return {
        method      : 'POST',
        credentials : 'include',
        body        : JSON.stringify(data),
        headers     : {'Content-type' : 'application/json; charset=UTF-8'}
    };

}