const when_ready = () => {

    if (($('#posted_form').val() === 'login')) open_login();

    if (($('#posted_form').val() === 'register')) open_register();
    
}

const open_login = () => {

    $('#modal_login_window').modal('show');
}

const open_register = () => {

    $('#modal_register_window').modal('show');
}

$('document').ready(when_ready);