<?php 

    class Validator {

        public static function validValue($value) {
            return isset($value) && !empty($value);
        }

        # Sanitize input
        public static function sanitize($value, $sanitize_function) {
            return filter_var($value, $sanitize_function);
        }

        # Check input if valid
        public static function validate($value, $validate_function) {
            return filter_var($value, $validate_function);
        }

    }

?>