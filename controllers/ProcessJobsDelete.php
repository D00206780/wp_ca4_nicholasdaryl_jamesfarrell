<?php require_once('models/Job.php'); ?>

<?php return function($req, $res) {

    # Store a connection to the db
    $app_db_connection = \Rapid\Database::getPDO();
    
    # If not logged in -
    if (!$req->session('user_id')) {

        # - Redirect
        $res->redirect('/');
    }

    # If the job has not been set
    if (!empty($req->query('id'))) {

        # Store job
        $job = Job::getJobById($app_db_connection, $req->query('id'));

        # If job stored -
        if ($job) {

            # - Delete job
            $job->delete($app_db_connection);

        } 

    }

    # Redirect
    $res->redirect('/view_jobs_page');

} ?>