<?php require_once('models/JobApplication.php'); ?>
<?php require_once('models/User.php'); ?>
<?php require_once('models/Job.php'); ?>

<?php return function($req, $res) {

    # Store a connection to the db
    $app_db_connection = \Rapid\Database::getPDO();

    # If not logged in -
    if (!$req->session('user_id')) {

        # - Redirect to home
        $res->redirect('/');
    }

    # Retrieve user
    $user = User::getUserById($app_db_connection, $req->session('user_id'));

    # If not employer
    if (!$user->get('account_type')) {
        
        $jobApplicationsFinal = [];

        # Store job applications
        $jobApplications = JobApplication::getAllJobApplicationsFromAUser($app_db_connection, $req->session('user_id'));

        # Iterate over applications
        foreach ($jobApplications as $jobApplication) {
            
            $job = Job::getJobById($app_db_connection, $jobApplication->get('job_id'));
            $status = $jobApplication->get('status');

            array_push($jobApplicationsFinal, ['job' => $job, 'status' => $status]);
        }

        $res->render('main', 'applications', [
            'jobApplications' => $jobApplicationsFinal,
        ]);

    } else {

        $jobsForThisEmployer = [];
        
        // get all jobs ever posted by this employer
        $jobsForThisEmployer = Job::getAllJobsByUserId($app_db_connection, $req->session('user_id'));

        $jobIds = [];
        
        // find out each of those ids
        foreach ($jobsForThisEmployer as $job) {
            array_push($jobIds, $job->get('id'));
        }

        $jobApplicationsRelatedToThisEmployer = [];
        // add all applications for jobs that has been posted by this employer to an array 
        foreach ($jobIds as $job_id) {
            
            array_push($jobApplicationsRelatedToThisEmployer, JobApplication::getAllJobApplicationsByJobId($app_db_connection, $job_id));
        }

        // add to an array with the job and user related to the $jobApplicationsRelatedToThisEmployer
        $jobApplicationsFinal = [];

        foreach ($jobApplicationsRelatedToThisEmployer as $jobApplications) {

            foreach ($jobApplications as $jobApplication) {

                //check if already accepted or rejected
                if ($jobApplication->get('status') == 0) {

                    // get user related to this application
                    $userOfThisApplication = User::getUserById($app_db_connection, $jobApplication->get('user_id'));

                    // get job related to this application
                    $jobOfThisApplication = job::getJobById($app_db_connection, $jobApplication->get('job_id'));

                    // add to array to be sent to view
                    array_push($jobApplicationsFinal, ['job' => $jobOfThisApplication, 'user' => $userOfThisApplication]);
                }
            }
        }

        $res->render('main', 'applicationsForEmployers', [
            'jobApplications' => $jobApplicationsFinal ?? NULL,
            'account_type'    => $req->session('account_type'),
        ]);
    }

} ?>