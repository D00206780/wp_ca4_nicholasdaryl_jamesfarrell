<?php require_once('models/User.php'); ?>
<?php require_once('models/Job.php'); ?>
<?php require_once('models/JobIndustry.php'); ?>
<?php require_once('models/JobCategory.php'); ?>

<?php return function($req, $res) {

    # Store a connection to the db
    $app_db_connection = \Rapid\Database::getPDO();

    # If not logged in -
    if (!$req->session('user_id')) {

        # - Redirect to home
        $res->redirect('/');
    }

    try {

        # Store jobs
        $jobs = Job::getAllJobs($app_db_connection);

    } catch (Exception $e) {

        # Store error
        $error = $e->getMessage();

    }

    # Display jobs page
    $res->render('main', 'jobs', [
        'jobs'          => $jobs                          ?? NULL,
        'user_id'       => $req->session('user_id')       ?? NULL,
        'account_type'  => $req->session('account_type')  ?? NULL,
        'jobs_error'    => $error                         ?? NULL,
    ]);

} ?>

