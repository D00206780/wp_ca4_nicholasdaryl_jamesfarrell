<?php require_once('models/JobApplication.php'); ?>

<?php return function($req, $res) {

    # Store a connection to the db
    $app_db_connection = \Rapid\Database::getPDO();
    
    # If not logged in -
    if (!$req->session('user_id')) {

        # - Redirect to home
        $res->redirect('/');
    }

    try {

        JobApplication::createJobApplication($app_db_connection, $req->session('user_id'), $req->query('job_id'));

        # - Redirect with success message
        $res->redirect('/applications?success=1');

    } catch (Exception $e) {

        # Store error
        $error = $e->getMessage();

    }

    # Display job form
    $res->render('main', 'job_form', [
        'job'               => $job          ?? NULL,
        'industries'        => $industries   ?? NULL,
        'categories'        => $categories   ?? NULL,
        'job_form_error'    => $error        ?? NULL,
    ]);

} ?>