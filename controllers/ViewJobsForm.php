<?php require_once('models/User.php'); ?>
<?php require_once('models/Job.php'); ?>
<?php require_once('models/JobIndustry.php'); ?>
<?php require_once('models/JobCategory.php'); ?>

<?php return function($req, $res) {

    # Store a connection to the db
    $app_db_connection = \Rapid\Database::getPDO();
    

    if ($req->session('user_id')) {

        $user = User::getUserById($app_db_connection, $req->session('user_id'));

    } else {

        $res->redirect('/');

    }

    # If not logged in -
    if (!$req->session('user_id') || !$user->get('account_type')) {

        # - Redirect
        $res->redirect('/');
    }

    try { 

        # Store job
        $job = Job::getJobById($app_db_connection, $req->query('id') ?? NULL) ?? NULL;

        # If successful -
        if ($job) {

            # - Store industry and category details
            $job_industry = JobIndustry::getIndustryById($app_db_connection, $job->get('industry_id') ?? NULL) ?? NULL;
            $job_category = JobCategory::getCategoryById($app_db_connection, $job->get('category_id') ?? NULL) ?? NULL;

            # - Store job properties
            $job_properties = [
                'id'              => $job->get('id')            ?? NULL,
                'title'           => $job->get('title')         ?? NULL,
                'company'         => $job->get('company')       ?? NULL,
                'description'     => $job->get('description')   ?? NULL,
                'location'        => $job->get('location')      ?? NULL,
                'rate'            => $job->get('rate')          ?? NULL,
                'term'            => $job->get('term')          ?? NULL,        
                'industry_id'     => $job_industry->get('id')   ?? NULL,
                'industry_name'   => $job_industry->get('name') ?? NULL,
                'category_id'     => $job_category->get('id')   ?? NULL,
                'category_name'   => $job_category->get('name') ?? NULL,
            ];

        }

        # Store industry and category information
        $industries   = JobIndustry::getAllIndustries($app_db_connection) ?? NULL;
        $categories   = JobCategory::getAllCategories($app_db_connection) ?? NULL;

    } catch (Exception $e) {

        # Store error
        $error = $e->getMessage();

    }
    
    # Display job form
    $res->render('main', 'job_form', [
        'job'             => $job_properties          ?? NULL,
        'industries'      => $industries              ?? NULL,
        'categories'      => $categories              ?? NULL,
        'jobs_form_error' => $error                   ?? NULL,
        'operation'       => $req->query('operation') ?? NULL,
    ]);

} ?>

