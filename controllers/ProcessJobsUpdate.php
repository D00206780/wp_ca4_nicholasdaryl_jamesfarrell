<?php require_once('models/Job.php'); ?>
<?php require_once('models/JobIndustry.php'); ?>
<?php require_once('models/JobCategory.php'); ?>

<?php return function($req, $res) {

    # Store a connection to the db
    $app_db_connection = \Rapid\Database::getPDO();

    # If not logged in -
    if (!$req->session('user_id')) {
        
        # - Redirect to home
        $res->redirect('/');
    }

    # If job not set -
    if (!$req->query('id')) {

        # - Redirect to jobs
        $res->redirect('/view_jobs_page');
    }

    try {

        # Store industry and category information
        $industries   = JobIndustry::getAllIndustries($app_db_connection) ?? NULL;
        $categories   = JobCategory::getAllCategories($app_db_connection) ?? NULL;   

        # Retrieve job
        $job = Job::getJobById($app_db_connection, $req->query('id') ?? NULL);

        # If successful -
        if ($job) {
            
            # - Store user details
            $job_industry = JobIndustry::getIndustryById($app_db_connection, $job->get('industry_id')    ?? NULL);
            $job_category = JobCategory::getCategoryById($app_db_connection, $job->get('category_id') ?? NULL);

            # - Update job
            $job->setTitle($req->body('title')              ?? NULL);
            $job->setCompany($req->body('company')          ?? NULL);
            $job->setDescription($req->body('description')  ?? NULL);
            $job->setLocation($req->body('location')        ?? NULL);
            $job->setTerm($req->body('term')                ?? NULL);
            $job->setRate($req->body('rate')                ?? NULL);
            $job->setIndustryId($req->body('industry_id')   ?? NULL);
            $job->setCategoryId($req->body('category_id')   ?? NULL);

            # - Save job
            $save_job = $job->save($app_db_connection);

            # If successful -
            if ($save_job) {

                # - Redirect with success message
                $res->redirect('/view_jobs_page?success');

            }

        }

    } catch (Exception $e) {

        # Store error
        $error = $e->getMessage();

    }

    # Redirect
    $res->redirect('/view_jobs_form?id=' . $req->query('id') . '&operation=update&error=' . $error);

} ?>