<?php require_once('models/CV.php'); ?>
<?php require_once('models/User.php'); ?>
<?php require_once('models/UserExperience.php'); ?>
<?php require_once('models/UserEducation.php'); ?>
<?php require_once('models/UserReferees.php'); ?>
<?php require_once('models/UserSkills.php'); ?>

<?php return function($req, $res) {

    # Store a connection to the db
    $app_db_connection = \Rapid\Database::getPDO();

    # If not logged in -
    if (!$req->session('user_id')) {

        # - Redirect to home
        $res->redirect('/');
    }
    
    # Delete operations
    if (!empty($req->query('delete'))) {

        try { 

            # Store data
            $user_experience = UserExperience::getAllExperienceByUserId($app_db_connection, $req->session('user_id'));
            $user_education  = UserEducation::getAllEducationByUserId($app_db_connection, $req->session('user_id'));
            $user_referees   = UserReferees::getAllRefereesByUserId($app_db_connection, $req->session('user_id'));
            $user_skills     = UserSkills::getAllSkillsByUserId($app_db_connection, $req->session('user_id'));

            # Store query field
            $delete_field = $req->query('delete');

            if ($delete_field === 'skill') {

                foreach ($user_skills as $user_skill) {
                    if ($user_skill->get('id') == $req->query('id')) {
                        $user_skill->delete($app_db_connection);
                    }
                }

                $res->redirect('/cv');

            } else if ($delete_field === 'education') {

                foreach ($user_education as $education) {
                    if ($education->get('id') == $req->query('id')) {
                        $education->delete($app_db_connection);
                    }
                }
                
                $res->redirect('/cv');

            } else if ($delete_field === 'experience') {

                foreach ($user_experience as $experience) {
                    if ($experience->get('id') == $req->query('id')) {
                        $experience->delete($app_db_connection);
                    }
                }
                
                $res->redirect('/cv');

            } else if ($delete_field === 'referee') {

                foreach ($user_referees as $user_referee) {
                    if ($user_referee->get('id') == $req->query('id')) {
                        $user_referee->delete($app_db_connection);
                    }
                }
                
                $res->redirect('/cv');

            }

        } catch (Exception $e) {

            $e->getMessage();

        }

    } else {

        try {

            # Add skills
            for ($i = 0; $i < $req->body('user_skills_count'); $i++) {
                
                $user_skill = UserSkills::getSkillById($app_db_connection, $req->body('skill_' . $i . '_id'));
                $user_skill->setName($req->body('skill_' . $i));
                $user_skill->save($app_db_connection);

            }

            # Add education
            for ($i = 0; $i < $req->body('user_education_count'); $i++) {
            
                $user_education = UserEducation::getEducationById($app_db_connection, $req->body('education_' . $i . '_id'));
                $user_education->setLevel($req->body('educationLevel_' . $i));
                $user_education->setTitle($req->body('educationTitle_' . $i));
                $user_education->setAwardingBody($req->body('educationAwardBody_' . $i));
                $user_education->setYearReceived($req->body('educationYear_' . $i));
                $user_education->save($app_db_connection);
            
            }

            # Add experience
            for ($i = 0; $i < $req->body('user_experience_count'); $i++) {
                
                $user_experience = UserExperience::getExperienceById($app_db_connection, $req->body('experience_' . $i . '_id'));
                $user_experience->setTerm($req->body('experienceTerm_' . $i));
                $user_experience->setPosition($req->body('experiencePosition_' . $i));
                $user_experience->setCompanyName($req->body('experienceCompany_' . $i));
                $user_experience->setDescription($req->body('experienceDescription_' . $i));
                $user_experience->save($app_db_connection);

            }

            # Add referees
            for ($i = 0; $i < $req->body('user_referees_count'); $i++) {

                $user_referee = UserReferees::getRefereeById($app_db_connection, $req->body('referee_' . $i . '_id'));
                $user_referee->setName($req->body('refereeName_' . $i));
                $user_referee->setContactNumber($req->body('refereeContact_' . $i));
                $user_referee->setEmailAddress($req->body('refereeEmail_' . $i));
                $user_referee->setDescription($req->body('refereeDescription_' . $i));
                $user_referee->save($app_db_connection);

            }

        } catch (Exception $e) {

            $e->getMessage();

        }
        
        # Redirect
        $res->redirect('/cv');
    }

} ?>