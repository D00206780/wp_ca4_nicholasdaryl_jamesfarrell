<?php return function($req, $res) {

    # If logged in -
    if ($req->session('user_id')) {
        
        # - Redirect to profile
        $res->redirect('/view_profile_page');
    }

    # Render the home page
    $res->render('home', 'home', [
        'key' => 'value',
    ]);
  
} ?>