<?php require_once('models/User.php'); ?>
<?php require_once('models/CV.php'); ?>

<?php return function($req, $res) {
    
    # Store a connection to the db
    $app_db_connection = \Rapid\Database::getPDO();

    # If not logged in -
    if (!$req->session('user_id')) {

        # - Redirect to home
        $res->redirect('/');
    }
    
    # Store user
    $user = User::getUserById($app_db_connection, $req->session('user_id'));

    # Edit CV
    if (!empty($req->query('edit')) && $user->get('account_type') == 0) {

        # Store CV
        $cv = CV::getCVArray($app_db_connection, $user->get('id'));

        # Render Edit CV Form
        $res->render('main', 'cvForm', [
            'cv'      => $cv,
            'user_id' => $user->get('id'),
        ]);

    } else if ($user->get('account_type') == 0) {

        # Store CV
        $cv = CV::getCVArray($app_db_connection, $user->get('id'));

        # View and Edit CV (Job Seekers)
        $res->render('main', 'cv', [
            'cv'      => $cv,
            'user_id' => $user->get('id'),
        ]);

    } else {

        if (!$req->query('applicantUserId')) {

            $res->redirect('/applications');

        }

        $applicantUserId = $req->query('applicantUserId') ?? 0; 
        $applicantCvId   = $req->query('applicantCvId')   ?? 0;

        // get the details about the applicants cv
        $cv = CV::getCVArray($app_db_connection, $applicantUserId);

        // Will allow employers to view an applicants CV without the ability to edit it
        $res->render('main', 'cvForEmployer', [
            'cv'           => $cv,
            'account_type' => $req->session('account_type'),
        ]);
    }

} ?>