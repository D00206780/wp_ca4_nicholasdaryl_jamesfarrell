<?php require_once('models/User.php'); ?>
<?php require_once('models/CV.php'); ?>

<?php return function($req, $res) {

    # Store a connection to the db
    $app_db_connection = \Rapid\Database::getPDO();

    # If not logged in -
    if (!$req->session('user_id')) {

        # - Redirect to home
        $res->redirect('/');
    }

    # Check form type
    if ($req->query('type') === 'introduction') {

        $res->render('main', 'addUserCv', [

        ]);

    } else if ($req->query('type') === 'skill') {

        $res->render('main', 'addUserSkill', [

        ]);

    } else if ($req->query('type') === 'education') {

        $res->render('main', 'addUserEducation', [

        ]);

    } else if ($req->query('type') === 'experience') {

        $res->render('main', 'addUserExperience', [

        ]);

    } else if ($req->query('type') === 'referee') {

        $res->render('main', 'addUserReferee', [
            
        ]);

    }

} ?>