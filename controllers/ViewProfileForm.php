<?php require_once('models/User.php'); ?>
<?php require_once('models/UserTitle.php'); ?>
<?php require_once('models/UserGender.php'); ?>

<?php return function($req, $res) {

    # Store a connection to the db
    $app_db_connection = \Rapid\Database::getPDO();

    # If not logged in -
    if (!$req->session('user_id')) {

        # - Redirect to home
        $res->redirect('/');
    }

    try {

        # Store user
        $user = User::getUserById($app_db_connection, $req->session('user_id') ?? NULL);

        # If stored -
        if ($user) {

            # - Store user title and gender details
            $user_title  = UserTitle::getTitleById($app_db_connection, $user->get('title_id')    ?? NULL) ?? NULL;
            $user_gender = UserGender::getGenderById($app_db_connection, $user->get('gender_id') ?? NULL) ?? NULL;

            # If stored -
            if ($user_title) {

                # - Store title name and id
                $title_id   = $user_title->get('id')       ?? NULL;
                $title_name = $user_title->get('name')     ?? NULL;
            }

            # If stored -
            if ($user_gender) {

                # - Store gender name and id
                $gender_id   = $user_gender->get('id')     ?? NULL;
                $gender_name = $user_gender->get('name')   ?? NULL;
            }

            # Store user properties
            $user_properties = [
                'title_id'       => $title_id                    ?? NULL,
                'title_name'     => $title_name                  ?? NULL,
                'first_name'     => $user->get('first_name')     ?? NULL,
                'last_name'      => $user->get('last_name')      ?? NULL,
                'gender_id'      => $gender_id                   ?? NULL,
                'gender_name'    => $gender_name                 ?? NULL,
                'contact_number' => $user->get('contact_number') ?? NULL,
                'email_address'  => $user->get('email_address')  ?? NULL,
                'address_line_1' => $user->get('address_line_1') ?? NULL,
                'address_line_2' => $user->get('address_line_2') ?? NULL,
                'town_city'      => $user->get('town_city')      ?? NULL,
                'county'         => $user->get('county')         ?? NULL,
                'country'        => $user->get('country')        ?? NULL,
                'account_type'   => $user->get('account_type')   ?? NULL,
            ];

        }

        # Store title and gender information
        $titles      = UserTitle::getAllTitles($app_db_connection)   ?? NULL;  
        $genders     = UserGender::getAllGenders($app_db_connection) ?? NULL;

    } catch (Exception $e) {

        # Store error
        $error = $e->getMessage();
    }

    # Display profile form
    $res->render('main', 'profile_form', [
        'user'               => $user_properties ?? NULL,
        'titles'             => $titles          ?? NULL,
        'genders'            => $genders         ?? NULL,
        'profile_form_error' => $error           ?? NULL,
    ]);
    
} ?>