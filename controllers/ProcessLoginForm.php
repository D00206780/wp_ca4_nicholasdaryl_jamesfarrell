<?php require_once('models/User.php'); ?>

<?php return function($req, $res) {

    # Store a connection to the db
    $app_db_connection = \Rapid\Database::getPDO();
    
    # If logged in -
    if ($req->session('user_id')) {

        # - Redirect
        $res->redirect('/view_profile_page');
    }

    try {
        
        # Store user
        $user = User::getUserByEmail($app_db_connection, $req->body('login_email_address'));

        # If stored -
        if ($user) {

            # - Check match
            $check_match = password_verify($req->body('login_password'), $user->get('hash'));

            # If matched -
            if ($check_match) {

                # - Store session
                $req->sessionSet('user_id',      $user->get('id'));
                $req->sessionSet('account_type', $user->get('account_type'));

                # - Redirect
                $res->redirect('/view_profile_page');
            }

        }
        

        # - Store error
        $error = 'Bad credentials!';

    } catch (Exception $e) {

        # Store error
        $error = $e->getMessage();

    }

    # Render
    $res->render('home', 'home', [
        'posted_form'        => 'login' ?? NULL,
        'modal_form_error'   => $error  ?? NULL,
    ]);

} ?>