<?php require_once('models/JobApplication.php'); ?>
<?php require_once('models/User.php'); ?>

<?php return function($req, $res) {

    # Store a connection to the db
    $app_db_connection = \Rapid\Database::getPDO();

    # If not logged in -
    if (!$req->session('user_id')) {

        # - Redirect to home
        $res->redirect('/');
    }

    # If not an employer -
    if (!$req->session('account_type')) {

        # - Redirect to home
        $res->redirect('/');

    }
    
    if (!empty($req->query('applicantUserId')) && !empty($req->query('jobId'))) {

        JobApplication::acceptJobApplication($app_db_connection, $req->query('applicantUserId'), $req->query('jobId'));

    }

    $res->redirect('/applications');

} ?>