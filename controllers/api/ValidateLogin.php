<?php require_once('models/User.php'); ?>

<?php return function($req, $res) { 

    # Store a connection to the db
    $app_db_connection = \Rapid\Database::getPDO();

    # Store RAW JSON Data
    $JSONBody = file_get_contents('php://input');
  
    # Convert JSON Data to an associative array
    $JSONBody = $JSONBody !== FALSE
        ? json_decode($JSONBody, TRUE)
        : NULL;

    # If set -
    if ($JSONBody) {

        # - Store errors
        $errors = [];

        try {

            # - Store user
            $user = User::getUserByEmail($app_db_connection, $JSONBody['emailAddress']);

            # If stored -
            if ($user) {

                # - Check match
                $check_match = password_verify($JSONBody['password'], $user->get('hash'));

                # If failed -
                if (!$check_match) {
                    
                    # - Store error
                    $errors += ['Incorrect email or password! Please try again.'];

                }

            } else {

                # - Store error
                $errors += ['Incorrect email or password! Please try again.'];

            }

        } catch (Exception $e) {

            # Store error
            $errors += $e->getMessage();

        }

        # Store errors
        if (!empty($errors)) $JSONBody['errors'] = $errors;

        # - Respond
        $res->json(json_encode($JSONBody));

    }
    
} ?>    