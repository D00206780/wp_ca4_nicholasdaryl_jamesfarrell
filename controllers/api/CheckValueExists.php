<?php require_once('models/Model.php'); ?>

<?php return function($req, $res) { 

    # Store a connection to the db
    $app_db_connection = \Rapid\Database::getPDO();

    # Store RAW JSON Data
    $JSONBody = file_get_contents('php://input');

    # Convert JSON Data to an associative array
    $JSONBody = $JSONBody !== FALSE
        ? json_decode($JSONBody, TRUE)
        : NULL;

    # If set -
    if ($JSONBody) {

        # - Store state
        $exists = false;

        try {

            # If exists -
            if (Model::checkIfExists($app_db_connection, $JSONBody['table'], $JSONBody['column'], $JSONBody['value'])) {

                # - Update state
                $exists = true;
            }

        } catch (Exception $e) {

            # Store error
            $errors = $e->getMessage();

        }
        
        # Store state
        $JSONBody['exists'] = $exists;

        # Store errors
        if (!empty($errors)) $JSONBody['errors'] = $errors;

        # Respond
        $res->json(json_encode($JSONBody));

    }

} ?>