<?php require_once('models/User.php'); ?>
<?php require_once('models/Model.php'); ?>

<?php return function($req, $res) { 

    # Store a connection to the db
    $app_db_connection = \Rapid\Database::getPDO();

    # Store RAW JSON Data
    $JSONBody = file_get_contents('php://input');
  
    # Convert JSON Data to an associative array
    $JSONBody = $JSONBody !== FALSE
        ? json_decode($JSONBody, TRUE)
        : NULL;

    # If set -
    if ($JSONBody) {

        # - Store errors
        $errors = [];

        try {

            # If exists -
            if (Model::checkIfExists($app_db_connection, 'users', 'email_address', $JSONBody['emailAddress'])) {

                # - Store message
                $errors += ['A user with that email already exists!'];
            }

            # Create user 
            $user = new User([
                'first_name'       => $JSONBody['firstName']    ?? NULL,
                'last_name'        => $JSONBody['lastName']     ?? NULL,
                'email_address'    => $JSONBody['emailAddress'] ?? NULL,
                'password'         => $JSONBody['password']     ?? NULL,
                'employer'         => $JSONBody['accountType']  ?? NULL,
            ]);

            # If failed -
            if (!$user) {

                # - Store error
                $errors += ['There was an error while processing your request!'];

            }
             
        } catch (Exception $e) {

            # Store error
            $errors += $e->getMessage();

        }

        # Store errors
        if (!empty($errors)) $JSONBody['errors'] = $errors;

        # Respond
        $res->json(json_encode($JSONBody));

    }
    
} ?>



    