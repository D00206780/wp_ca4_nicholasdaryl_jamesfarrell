<?php require_once('models/User.php'); ?>
<?php require_once('models/UserTitle.php'); ?>
<?php require_once('models/UserGender.php'); ?>

<?php return function($req, $res) {
    
    # Store a connection to the db
    $app_db_connection = \Rapid\Database::getPDO();

    # If not logged in -
    if (!$req->session('user_id')) {

        # - Redirect to home
        $res->redirect('/');
    }

    try {

        # Store user -
        $user = User::getUserById($app_db_connection, $req->session('user_id') ?? NULL);        

        # If stored -
        if ($user) {

            # - Update user
            $user->setTitleId($req->body('title_id')                ?? NULL);
            $user->setFirstName($req->body('first_name')            ?? NULL);
            $user->setLastName($req->body('last_name')              ?? NULL);
            $user->setGenderId($req->body('gender_id')              ?? NULL);
            $user->setContactNumber($req->body('contact_number')    ?? NULL);
            $user->setEmailAddress($req->body('email_address')      ?? NULL);
            $user->setAddressLine1($req->body('address_line_1')     ?? NULL);
            $user->setAddressLine2($req->body('address_line_2')     ?? NULL);
            $user->setTownCity($req->body('town_city')              ?? NULL);
            $user->setCounty($req->body('county')                   ?? NULL);
            $user->setCountry($req->body('country')                 ?? NULL);

            # Save user
            $save_user = $user->save($app_db_connection);

            # If successful -
            if ($save_user) {

                # - Redirect with success message
                $res->redirect('/view_profile_page?status=updated');

            }

            # - Redirect with success message
            $res->redirect('/view_profile_page?status=failed');

        } 

    } catch (Exception $e) {

        # Store error
        $error = $e->getMessage();
    }

    # Redirect
    $res->redirect('/view_profile_form?'. $error);

} ?>