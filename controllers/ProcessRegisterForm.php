<?php require_once('models/User.php'); ?>

<?php return function($req, $res) {

    # Store a connection to the db
    $app_db_connection = \Rapid\Database::getPDO();

    # If logged in -
    if ($req->session('user_id')) {

        # - Redirect
        $res->redirect('/view_profile_page');
    }

    try {

        # If exists -
        if (Model::checkIfExists($app_db_connection, 'users', 'email_address', $req->body('register_email_address'))) {

            # - Redirect
            $res->redirect('/?user_exists');
        }

        # Create user 
        $user = new User([
            'first_name'       => $req->body('register_first_name')    ?? NULL,
            'last_name'        => $req->body('register_last_name')     ?? NULL,
            'email_address'    => $req->body('register_email_address') ?? NULL,
            'password'         => $req->body('register_password')      ?? NULL,
            'account_type'     => $req->body('register_account_type')  ?? NULL,
        ]);

        # Save user
        $save_user = $user->save($app_db_connection);

        # If successful -
        if ($save_user) {
            
            # - Store session
            $req->sessionSet('user_id',      $user->get('id'));
            $req->sessionSet('account_type', $user->get('account_type'));

            # - Redirect
            $res->redirect('/view_profile_page');
        } 

    } catch (Exception $e) {
       
        # Store error
        $error = $e->getMessage();
    }

    # Render
    $res->render('home', 'home', [
        'posted_form'        => 'register' ?? NULL,
        'modal_form_error'   => $error     ?? NULL,
    ]);

} ?>