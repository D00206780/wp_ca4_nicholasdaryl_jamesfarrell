<?php require_once('models/JobApplication.php'); ?>

<?php return function($req, $res) {

    # Store a connection to the db
    $app_db_connection = \Rapid\Database::getPDO();
    
    # If not logged in -
    if (!$req->session('user_id')) {

        # - Redirect to home
        $res->redirect('/');
    }

    if (!empty($req->query('id'))) {

        JobApplication::deleteJobApplication($app_db_connection, $req->session('user_id'), $req->query('id'));

    }

    $res->redirect('/applications');

} ?>