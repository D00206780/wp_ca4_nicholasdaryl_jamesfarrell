<?php require_once('models/Job.php'); ?>
<?php require_once('models/JobIndustry.php'); ?>
<?php require_once('models/JobCategory.php'); ?>

<?php return function($req, $res) {

    # Store a connection to the db
    $app_db_connection = \Rapid\Database::getPDO();

    # If not logged in -
    if (!$req->session('user_id')) {

        # - Redirect to home
        $res->redirect('/');
    }

    # If not set -
    if (!$req->query('id')) {

        # - Redirect 
        $res->redirect('view_jobs_page');
    }

    try {

        # Store job
        $job = Job::getJobById($app_db_connection, $req->query('id'));

    } catch (Exception $e) {

        # Store error
        $error = $e->getMessage();
    }

    # Display jobs page
    $res->render('main', 'job', [
        'job'          => $job          ?? NULL,
        'job_error'    => $error        ?? NULL,
    ]);

} ?>

