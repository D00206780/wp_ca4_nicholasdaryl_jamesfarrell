<?php require_once('models/Job.php'); ?>
<?php require_once('models/JobIndustry.php'); ?>
<?php require_once('models/JobCategory.php'); ?>

<?php return function($req, $res) {

    # Store a connection to the db
    $app_db_connection = \Rapid\Database::getPDO();
    
    # If not logged in -
    if (!$req->session('user_id')) {

        # - Redirect
        $res->redirect('/');
    }

    try {

        # Store industry and category information
        $industries   = JobIndustry::getAllIndustries($app_db_connection) ?? NULL;
        $categories   = JobCategory::getAllCategories($app_db_connection) ?? NULL;      

        # Create job
        $job = new Job([
            'title'         => $req->body('title')       ?? NULL,
            'company'       => $req->body('company')     ?? NULL,
            'description'   => $req->body('description') ?? NULL,
            'location'      => $req->body('location')    ?? NULL,
            'term'          => $req->body('term')        ?? NULL,
            'rate'          => $req->body('rate')        ?? NULL,
            'user_id'       => $req->session('user_id')  ?? NULL,
            'industry_id'   => $req->body('industry_id') ?? NULL,
            'category_id'   => $req->body('category_id') ?? NULL,
        ]);

        # Save job
        $save_job = $job->save($app_db_connection);

        # If successful -
        if ($save_job) {

            # - Redirect
            $res->redirect('/view_jobs_page?success');
        }

    } catch (Exception $e) {

        # Store error
        $error = $e->getMessage();

    }

    # Display job form
    $res->render('main', 'job_form', [
        'job'               => $job          ?? NULL,
        'industries'        => $industries   ?? NULL,
        'categories'        => $categories   ?? NULL,
        'job_form_error'    => $error        ?? NULL,
        'operation'         => 'create'      ?? NULL,
    ]);

} ?>