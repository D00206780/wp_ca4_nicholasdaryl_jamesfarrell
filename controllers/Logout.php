<?php return function($req, $res) {

    # Destroy session
    $req->sessionDestroy();
    
    # Redirect to home
    $res->redirect('/');

} ?>