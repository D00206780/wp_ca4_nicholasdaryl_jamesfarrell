<?php return function($req, $res) {

    //Will process the create and update forms in the CV view

    require_once('models/JobApplication.php');
    require_once('models/User.php');

    $req->sessionStart();

    # Store a connection to the db
    $app_db_connection = \Rapid\Database::getPDO();

    $user_id = $req->session('user_id') ?? NULL;

    $user = User::getUserById($app_db_connection, $user_id);

    if (!$user_id || $user->get('account_type') == 0) {
        $res->redirect("/");
    }
    
    if (!empty($req->query('applicantUserId')) && !empty($req->query('jobId'))) {
        JobApplication::rejectJobApplication($app_db_connection, $req->query('applicantUserId'), $req->query('jobId'));
    }

    $res->redirect('/applications');
} ?>