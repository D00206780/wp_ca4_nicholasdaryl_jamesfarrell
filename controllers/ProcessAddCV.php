<?php require_once('models/CV.php'); ?>
<?php require_once('models/User.php'); ?>
<?php require_once('models/UserEducation.php'); ?>
<?php require_once('models/UserExperience.php'); ?>
<?php require_once('models/UserSkills.php'); ?>
<?php require_once('models/UserReferees.php'); ?>

<?php return function($req, $res) {
   
    # Store a connection to the db
    $app_db_connection = \Rapid\Database::getPDO();

    # If not logged in -
    if (!$req->session('user_id')) {

        # - Redirect to home
        $res->redirect('/');
    }

    try {

        # Check type
        if (!empty($req->body('introduction'))) {
            
            $properties = [
                'user_id'           => $req->session('user_id'),
                'introduction'      => $req->body('introduction'),
            ];

            $cv = new CV($properties);
            $cv->save($app_db_connection);
            
        } else if (!empty($req->body('skill'))) {
            
            $properties = [
                'user_id'           => $req->session('user_id'),
                'name'              => $req->body('skill'),
            ];

            $user_skills = new UserSkills($properties);
            $user_skills->save($app_db_connection);
            
        } else if (!empty($req->body('educationTitle'))) {
            
            $properties = [
                'user_id'           => $req->session('user_id'),
                'title'             => $req->body('educationTitle'),
                'level'             => $req->body('educationLevel'),
                'awarding_body'     => $req->body('educationAwardBody'),
                'year_received'     => $req->body('educationYear'),
            ];

            $user_education = new UserEducation($properties);
            $user_education->save($app_db_connection);

        } else if (!empty($req->body('experiencePosition'))) {

            $properties = [
                'user_id'           => $req->session('user_id'),
                'position'          => $req->body('experiencePosition'),
                'term'              => $req->body('experienceTerm'),
                'company_name'      => $req->body('experienceCompany'),
                'description'       => $req->body('experienceDescription'),
            ];

            $user_experience = new UserExperience($properties);
            $user_experience->save($app_db_connection);

        } else if (!empty($req->body('refereeName'))) {

            $properties = [
                'user_id'           => $req->session('user_id'),
                'name'              => $req->body('refereeName'),
                'email_address'     => $req->body('refereeEmail'),
                'contact_number'    => $req->body('refereeContact'),
                'description'       => $req->body('refereeDescription'),
            ];

            $user_referees = new UserReferees($properties);
            $user_referees->save($app_db_connection);

        }

    } catch (Exception $e) {

        $e->getMessage();

    }


    $res->redirect('/cv');

} ?>